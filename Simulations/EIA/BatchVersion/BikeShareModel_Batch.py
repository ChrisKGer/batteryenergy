import numpy as np
import pandas as pd
import pickle
import os

Areas66Energy = pickle.load(open("Areas66Energy.p",'rb'))
resultArr = pickle.load(open("resultArr.p",'rb'))
dateListUnique = pickle.load(open("dateListUnique.p",'rb'))
CurrentRun = os.environ['SGE_TASK_ID']
CurrentRun = int(CurrentRun)
startRange = CurrentRun*1000
if CurrentRun < (int(Areas66Energy.shape[0]/1000)+1):
    stopRange = (CurrentRun+1)*1000
else:
    stopRange = Areas66Energy.shape[0]

def find(lst, a):
    return [i for i, x in enumerate(lst) if x==a]

#Enter all the fitting values, if you cant find anything, paste that
for r in range(startRange,stopRange):
    value = Areas66Energy.iloc[r,0]
    print(r)
    for c in range(Areas66Energy.shape[1]-1):
        try:
            row = find([x[0] for x in resultArr[:,c]],value)[0]
            Areas66Energy.iloc[r,c+1] = resultArr[row,c][1]
        except:
            Areas66Energy.iloc[r,c+1] = "NA"
pickle.dump(Areas66Energy.iloc[startRange:stopRange,:], open( str(CurrentRun)+".p", "wb" ) )

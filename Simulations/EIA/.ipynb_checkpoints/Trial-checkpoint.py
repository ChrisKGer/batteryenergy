import numpy as np
import json
import pandas as pd
from pandas.io.json import json_normalize
import requests
import pickle
import time
import xarray as xr

myKey = "db979b2f848bd9810ad3b5604d770ce1"
def addAPI(apiUrl,apiKey=myKey):
    return apiUrl.replace("YOUR_API_KEY_HERE",myKey)
#statesAPIArr = np.empty(shape=(54,10),dtype="U128") #128 chars
#resultArr = np.empty(shape=(54,10),dtype="object") #Anything
#seriesCollection = [None]*2

requestPage = "http://api.eia.gov/category/?api_key=YOUR_API_KEY_HERE&category_id=2122628"
requestPage = addAPI(requestPage)

response = requests.get(requestPage)
print(response.status_code)
data = json.loads(response.text)

NumRes = len(data["category"]["childseries"])
ElecRegions = np.empty(shape=(NumRes,1),dtype="U128")
for i in range(NumRes):
    ElecRegions[i] = data['category']['childseries'][i]['series_id']
    
APIDataStorage = []
for i in range(NumRes):
    requestPage = RequestStump + str(ElecRegions[i][0])
    response = requests.get(requestPage)
    print(str(i) + str(response.status_code))
    data2 = json.loads(response.text)
    APIDataStorage.append(data2)
    time.sleep(2)

pickle.dump(APIDataStorage, open( "APIDataStorage.p", "wb" ) )

max = 0
for i in range(NumRes):
    length = len(APIDataStorage[i]["series"][0]['data'])
    if length > max:
        max= length

resultArr = np.empty(shape=(max+2,NumRes*2),dtype="object")
columnArr = [APIDataStorage[2]["series"][0]["units"]]
for c in range(NumRes):
    columnArr.append(APIDataStorage[c]["series"][0]["name"])
    resultArr[0,2*c] = APIDataStorage[c]["series"][0]["name"]
    resultArr[1,2*c] = APIDataStorage[c]["series"][0]["units"]
    for r in range(max):
        try:
            resultArr[r+2,2*c] =  APIDataStorage[c]["series"][0]['data'][r][0]
            resultArr[r+2,2*c+1] =  APIDataStorage[c]["series"][0]['data'][r][1]
        except:
            resultArr[r+2,c] = "failed"

#Make unique list of all available dates
dateList = []
for r in range(resultArr.shape[0]):
    for c in range(0,resultArr.shape[1],2):
        dateList.append(resultArr[r,c])
    
dateListUnique = set(dateList)
dateListUnique = [item for item in dateListUnique if not None or len(item)==12]

#Enter all the fitting values, if you cant find anything, paste that
def find(lst, a):
    return [i for i, x in enumerate(lst) if x==a]
Areas66Energy = np.empty(shape=(len(dateListUnique), NumRes+1),dtype="object")
Areas66Energy = pd.DataFrame(Areas66Energy)
Areas66Energy.columns = columnArr
Areas66Energy.iloc[:,0] = [*list(dateListUnique)]
for r in range(Areas66Energy.shape[0]):
    value = Areas66Energy.iloc[r,0]
    print(r)
    for c in range(Areas66Energy.shape[1]-1):
        row = find(resultArr[:,2*(c-1)-1],value)
        if len(row) == 1:
            Areas66Energy.iloc[r,c+1] = resultArr[row,2*c+1]
        else:
            Areas66Energy.iloc[r,c+1] = "NA"
            
pickle.dump(Areas66Energy, open( "Areas66Energy.p", "wb" ) )
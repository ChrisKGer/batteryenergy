import pandas as pd
import os
import pickle
import numpy as np
import time as time
import sys

GenAvg = pickle.load(open("GenAvg.p",'rb'))
PeakLimit = 2/3
QueueData = GenAvg.drop(GenAvg.columns[range(0,14)],axis=1)
peakTH = QueueData.Total.quantile(PeakLimit)
renewAvg = [QueueData.SolWind.mean(),QueueData.RenewInclHyd.mean(),QueueData.RenewExclHyd.mean()]
#Create demand rates that are dependent on it being a peak hour or not and average Sup Rate
QueueData["SolWindRate"] = (QueueData.SolWind/renewAvg[0])
QueueData["RenewInclRate"] = (QueueData.RenewInclHyd/renewAvg[1])
QueueData["RenewExclRate"] = (QueueData.RenewExclHyd/renewAvg[2])
QueueData["DemRate"] = (QueueData.Total>=peakTH)*(1/(1-PeakLimit))#Rescale demand during peak time to average 1 unit acros all periods

#Function to create RateMatrix
def create_RateMatrix(lamb,mu,States):
    if States == 1:
        return np.eye(1)

    RateMatrix = np.eye(States)
    RateMatrix.ravel()[1:max(0,States-1)*States:States+1] = lamb
    RateMatrix.ravel()[States*1:States*(1+States):States+1] = mu
    np.fill_diagonal(RateMatrix,(-(lamb+mu)))
    RateMatrix[0,0]=-lamb
    RateMatrix[(States-1),(States-1)]=-mu

    return RateMatrix

def NHPP_Simul(BatterySize, intervalsPerHour, lambArr, muArr, approxIter):

    #Error Checking at its best
    try:
        if type(BatterySize) != int:
            return print("The BatterySize has to be an integer")
    except:
        return print("Provided the wrong data formate for the Battery Size. Please pass an integer to the function")

    try:
        if  type(intervalsPerHour) != int:
            return print("The intervals per hour have to be an integer")
    except:
        return print("Provided the wrong data formate for the intervals. Please pass an integer to the function")

    try:
        if len(lambArr) != len(muArr):
            return print("Supply and Demand rate are not of identical length")
    except:
        return print("Provided the wrong data format for the Rates. Please pass columns to the function")

    try:
        if  type(approxIter) != int:
            return print("Num of iterations for rate approximation have to be integer values")
    except:
        return print("Provided the wrong data format for the approximation runs. Please pass an integer to the function")


    #Adjust for the subperiod lengths
    BatterySize += 1 #Battery state spaces are one larger than the battery
    periods = len(lambArr)-1
    subperiods = periods*intervalsPerHour

    #Create all arrays needed during calculation
    loss = np.zeros(shape=(1,BatterySize))
    I = np.eye(BatterySize)
    Pis = np.zeros(BatterySize)
    Pis[BatterySize-1] = 1
    PiZeros = np.zeros(subperiods+1)
    PiZeros[0] = 0

    #Calculate All Transition Rates
    for subperiod in range(subperiods):
        hour = int(subperiod/intervalsPerHour)

        #Check if you need new ratematrix
        if subperiod%intervalsPerHour==0:
            RateMatrix = create_RateMatrix(lambArr[hour],muArr[hour],BatterySize)
            TransPr = np.linalg.matrix_power(I + 0.5*RateMatrix/intervalsPerHour/approxIter,approxIter)#Continuity Correction

        #Multiply new probabilities
        Pis = np.dot(Pis,TransPr)

        #Paste loss value
        PiZeros[subperiod+1] = Pis[0]

        #Calculate second half of period
        Pis = np.dot(Pis,TransPr)


    return PiZeros, PiZeros.mean()


CurrentRun = os.environ['SGE_TASK_ID']
CurrentRun = int(CurrentRun)
NumYears = 10
BatAt0Avg = np.zeros(shape=(1,3))
#Simulates this stuff, but for 10 years. #10 Years for 48 Battery Capacity takes about
start_time = time.time()
for SizeRun in range(CurrentRun,CurrentRun+1):

    #Run it with Num Years * Observations, iterate over all battery sizes with 10 subperiods per period...
    _ , BatAt0Avg[0,0] = NHPP_Simul(SizeRun,10,np.tile(QueueData.SolWindRate,NumYears),np.tile(QueueData.DemRate,NumYears),1024)
    _ , BatAt0Avg[0,1] = NHPP_Simul(SizeRun,10,np.tile(QueueData.RenewInclRate,NumYears),np.tile(QueueData.DemRate,NumYears),1024)
    _ , BatAt0Avg[0,2] = NHPP_Simul(SizeRun,10,np.tile(QueueData.RenewExclRate,NumYears),np.tile(QueueData.DemRate,NumYears),1024)
    print(str(SizeRun) + "--- %s seconds ---" % (time.time() - start_time))

pickle.dump( BatAt0Avg[0,:], open( "BatAt0Avg" + str(CurrentRun)+".p", "wb" ) )

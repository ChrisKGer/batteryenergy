#qsub -N BatchTry  -j Y -t 1:168 BikeShareParamEstimation.sh
#-m e -M ckaps@upenn.edu
source /opt/rh/rh-python36/enable #activate Python 3
cd ~/BatteryEnv #Switch into environment folder
source bin/activate #activate environment
cd ~/BatteryEnv/BikeShareModel #Switch To where the data is and where the output is
python3 ~/HPCC_Battery_Energy/Simulations/BikeShareParamEstimation.py #Run the python file

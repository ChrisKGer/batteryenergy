import pandas as pd
import os
import pickle
import numpy as np
import time as time
import sys
import math

GenAvg = pickle.load(open("GenAvg.p",'rb'))
PeakLimit = 2/3
QueueData = GenAvg.drop(GenAvg.columns[range(0,14)],axis=1)
peakTH = QueueData.Total.quantile(PeakLimit)
renewAvg = [QueueData.SolWind.mean(),QueueData.RenewInclHyd.mean(),QueueData.RenewExclHyd.mean()]
#Create demand rates that are dependent on it being a peak hour or not and average Sup Rate
QueueData["SolWindRate"] = (QueueData.SolWind/renewAvg[0])
QueueData["RenewInclRate"] = (QueueData.RenewInclHyd/renewAvg[1])
QueueData["RenewExclRate"] = (QueueData.RenewExclHyd/renewAvg[2])
QueueData["DemRate"] = (QueueData.Total>=peakTH)*(1/(1-PeakLimit))#Rescale demand during peak time to average 1 unit acros all periods

#Function to create RateMatrix
def create_RateMatrix(lamb,mu,States):
    if States == 1:
        return np.eye(1)

    RateMatrix = np.eye(States)
    RateMatrix.ravel()[1:max(0,States-1)*States:States+1] = lamb
    RateMatrix.ravel()[States*1:States*(1+States):States+1] = mu
    np.fill_diagonal(RateMatrix,(-(lamb+mu)))
    RateMatrix[0,0]=-lamb
    RateMatrix[(States-1),(States-1)]=-mu

    return RateMatrix

def period_Loss(Pis, currentLamb, AvgAbsLamb, currentMuAbs):
    #Treat all cases with the state-space being small
    if Pis.shape[0] >= 3:
        penaltyUnits = 3*Pis[0] + 2*Pis[1] + 1*Pis[2]
    elif Pis.shape[0] == 2:
        penaltyUnits = 3*Pis[0] + 2*Pis[1]
    else:
        penaltyUnits = 3

    #Deduct current Output
    penaltyUnits -= currentLamb

    #Look where in the Merit order I am an how much it would cost me
    meritOrderPoint = currentMuAbs+penaltyUnits*AvgAbsLamb
    meritOrderPrice = math.exp((math.sqrt(meritOrderPoint/10000))) #Take highest price to clear market
    totalPenalty = meritOrderPrice * penaltyUnits

    return totalPenalty

def NHPP_Simul(BatterySize, intervalsPerHour, lambArr, muArr, lambArrAbs, meritArrAbs, approxIter):

    #Error Checking at its best
    try:
        if type(BatterySize) != int:
            return print("The BatterySize has to be an integer")
    except:
        return print("Provided the wrong data formate for the Battery Size. Please pass an integer to the function")

    try:
        if  type(intervalsPerHour) != int:
            return print("The intervals per hour have to be an integer")
    except:
        return print("Provided the wrong data formate for the intervals. Please pass an integer to the function")

    try:
        if len(lambArr) != len(muArr)  != len(lambArrAbs) != len(meritArrAbs):
            return print("Supply and Demand rates are not of identical length")
    except:
        return print("Provided the wrong data format for the rates. Please pass columns to the function")

    try:
        if  type(approxIter) != int:
            return print("Num of iterations for rate approximation have to be integer values")
    except:
        return print("Provided the wrong data format for the approximation runs. Please pass an integer to the function")


    #Adjust for the subperiod lengths
    BatterySize += 1 #Battery state spaces are one larger than the battery
    periods = len(lambArr)-1
    subperiods = periods * intervalsPerHour

    #Create all arrays needed during calculation
    loss = np.zeros(shape=(1, BatterySize))
    I = np.eye(BatterySize)
    Pis = np.zeros(BatterySize)
    Pis[BatterySize-1] = 1
    PiZeros = np.zeros(subperiods+1)
    PiZeros[0] = 0 #Initialize at 0 periods with discharged
    AvgLamb = lambArrAbs.mean()
    Loss = 0 #Initialize at 0 periods with

    #Calculate All Transition Rates
    for subperiod in range(subperiods):
        hour = int(subperiod / intervalsPerHour)

        #Check if you need new ratematrix
        if subperiod%intervalsPerHour==0:
            RateMatrix = create_RateMatrix(lambArr[hour], muArr[hour], BatterySize)
            TransPr = np.linalg.matrix_power(I + 0.5 * RateMatrix / intervalsPerHour / approxIter, approxIter)#Continuity Correction

        #Multiply new probabilities
        Pis = np.dot(Pis, TransPr)

        #Paste loss value
        PiZeros[subperiod+1] = Pis[0]
        Loss += max(0,period_Loss(Pis, lambArr[hour], AvgLamb, meritArrAbs[hour]))

        #Calculate second half of period
        Pis = np.dot(Pis, TransPr)


    return PiZeros, PiZeros.mean(), Loss


CurrentRun = os.environ['SGE_TASK_ID']
CurrentRun = int(CurrentRun)
NumYears = 10
BatAt0Avg = np.zeros(shape=(1,3))
BatLosses = np.zeros(shape=(1,3))
#Simulates this stuff, but for 10 years. #10 Years for 48 Battery Capacity takes about
start_time = time.time()
for SizeRun in range(CurrentRun,CurrentRun+1):

    #Run it with Num Years * Observations, iterate over all battery sizes with 10 subperiods per period...
    _ , BatAt0Avg[0,0], BatLosses[0,0] = NHPP_Simul(SizeRun, 10, np.tile(QueueData.SolWindRate,NumYears), np.tile(QueueData.DemRate,NumYears), np.tile(QueueData.AbsSolWind,NumYears), np.tile(QueueData.AbsTotExclSolWind,NumYears), 1024)
    _ , BatAt0Avg[0,1], BatLosses[0,1] = NHPP_Simul(SizeRun, 10, np.tile(QueueData.RenewInclRate,NumYears), np.tile(QueueData.DemRate,NumYears), np.tile(QueueData.AbsRenewInclHyd,NumYears), np.tile(QueueData.AbsTotExclRenInclHyd,NumYears), 1024)
    _ , BatAt0Avg[0,2], BatLosses[0,2] = NHPP_Simul(SizeRun, 10, np.tile(QueueData.RenewExclRate,NumYears), np.tile(QueueData.DemRate,NumYears), np.tile(QueueData.AbsRenewExclHyd,NumYears), np.tile(QueueData.AbsTotExclRenExclHyd,NumYears), 1024)
    print(str(SizeRun) + "--- %s seconds ---" % (time.time() - start_time))

pickle.dump( BatAt0Avg[0,:], open( "BatAt0Avg" + str(CurrentRun)+".p", "wb" ) )
pickle.dump( BatLosses[0,:], open( "BatLosses" + str(CurrentRun)+".p", "wb" ) )

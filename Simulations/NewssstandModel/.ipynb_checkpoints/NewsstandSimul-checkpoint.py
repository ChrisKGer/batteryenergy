import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

r = 0.35
nDay = 14
nNight = 24 - nDay
nPeriods = 365*30

dDay = 150
dNight = 100
g = 20
pL = 35
pH = 55

e = .95
cS = 300_000 #cost per MWh
cG = 1_000_000 #cost per MWh

def calcProfit(K,Q,dDay,dNight, r, nDay, nPeriods, prodDay, prodNight):
    try:
        V = max(0,(Q-dNight))/ (Q-dDay)
    except:
        V = 0
    RevProdDay = min(Q, dDay)* pH * sum(prodDay)
    RevProdNight = min(Q, dNight) * pL * sum(prodNight)
    RevPenaltyDay = min(0, Q - dDay) * g * sum(prodDay) - dDay * g * (nDay*nPeriods - sum(prodDay))
    RevPenaltyNight = min(0, Q - dNight) * g * sum(prodNight) - dNight * g * (nNight * nPeriods - sum(prodNight))

    BatStoreDay = max(0, Q - dDay) * np.minimum(K*V, prodDay)
    BatStoreNight = max(0, Q - dNight) * np.minimum(K, prodNight)
    ShortageDay = (nDay - prodDay) * dDay + max(0, dDay - Q) * prodDay
    ShortageNight = (nNight - prodNight) * dNight + max(0, dNight - Q) * prodNight
    RevBatteryDay = sum(np.minimum(BatStoreNight, ShortageDay)) * pH * e
    RevBatteryNight = sum(np.minimum(BatStoreDay, ShortageNight)) * pL * e
    RevTotal = RevProdDay + RevProdNight + RevPenaltyDay + RevPenaltyNight + RevBatteryDay + RevBatteryNight
    CostTotal = -K * cS * max(0,Q-dNight)  - cG * Q
    ProfitTotal = RevTotal + CostTotal

    return(ProfitTotal, RevTotal, CostTotal)

ResArray = np.zeros(6000*3).reshape(-1,3)
counter = 0
prodDay = np.random.binomial(nDay, r, size=nPeriods)
prodNight = np.random.binomial(nNight, r, size=nPeriods)

for k in range(15):
    for q in range(400):
        ResArray[counter,0] = k
        ResArray[counter,1] = q
        ResArray[counter,2], _, _ = calcProfit(k, q, dDay, dNight, r, nDay, nPeriods, prodDay ,prodNight)
        counter += 1
        if counter%1000 == 0:
            print(counter)

#Scatter Plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(ResArray[:,0], ResArray[:,1], ResArray[:,2])
plt.show()

#Scatter Plot
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(ResArray[:,1], ResArray[:,0], ResArray[:,2])
plt.show()

#Surface Plot
fig = plt.figure()
ax = Axes3D(fig)
X,Y = np.meshgrid(range(15), range(400))
ax.plot_surface(X,Y, ResArray[:,2].reshape(400,15,order='F'))
plt.show()

#Contour Plot
fig = plt.figure()
ax = Axes3D(fig)
X,Y = np.meshgrid(range(15), range(400))
Z = ResArray[:,2].reshape(400,15,order='F')
minZ = min(ResArray[:,2])
maxZ = max(ResArray[:,2])
ax.plot_surface(X, Y, Z, rstride=5, cstride=5, alpha=0.3)
cset = ax.contour(X, Y, Z, zdir='z', offset=minZ, cmap=cm.coolwarm)
cset = ax.contour(X, Y, Z, zdir='x', offset=0, cmap=cm.coolwarm)
cset = ax.contour(X, Y, Z, zdir='y', offset=400, cmap=cm.coolwarm)

ax.set_xlabel('X')
ax.set_xlim(0, 15)
ax.set_ylabel('Y')
ax.set_ylim(0, 400)
ax.set_zlabel('Z')
ax.set_zlim(minZ, maxZ)
plt.show()



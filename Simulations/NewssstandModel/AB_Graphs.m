%Joint Equilibria Hard coded + calculate storage size
ErcotDL = 35000;
ErcotDH = 65000;
JointEqBatERCOT = [66500 0.22; 103000 2.61; 142000 3.26; 168500 3.58];
JointEqBatERCOT(:,3) = (JointEqBatERCOT(:,1)- ErcotDL) .* JointEqBatERCOT(:,2);
JointEqBatERCOT(:,4) = [0 50 100 150];
JointEqKraftERCOT = [80500 3.42; 135000 4.33; 172000 4.72 ; 196000 4.95];
JointEqKraftERCOT(:,3) = (JointEqKraftERCOT(:,1)- ErcotDL) .* JointEqKraftERCOT(:,2);
JointEqKraftERCOT(:,4) = [0 50 100 150];

GerDL = 48000;
GerDH = 72000;
JointEqBatGER = [126000 3.01; 175000 3.53; 205000 3.80; 227500 4.00] ;
JointEqBatGER(:,3) = (JointEqBatGER(:,1)- GerDL) .* JointEqBatGER(:,2);
JointEqBatGER(:,4) = [0 50 100 150];
JointEqKraftGER = [166000 4.67; 211000 5.00; 238000 5.20; 257000 5.35] ;
JointEqKraftGER(:,3) = (JointEqKraftGER(:,1)- GerDL) .* JointEqKraftGER(:,2);
JointEqKraftGER(:,4) = [0 50 100 150];

PjmDL = 84000;
PjmDH = 99000;
JointEqBatPJM = [100000 0.15; 112000 2.12; 207000 3.47; 275000 3.75] ;
JointEqBatPJM(:,3) = (JointEqBatPJM(:,1)- PjmDL) .* JointEqBatPJM(:,2);
JointEqBatPJM(:,4) = [0 50 100 150];
JointEqKraftPJM = [99000 3.00; 172000 4.62; 275000 5.12; 335000 5.33] ;
JointEqKraftPJM(:,3) = (JointEqKraftPJM(:,1)- PjmDL) .* JointEqKraftPJM(:,2);
JointEqKraftPJM(:,4) = [0 50 100 150];

%Battery only
f = figure('visible', 'on');
color = [1 0 0 ; 0.8500, 0.3250, 0.0980; 0.9290, 0.6940, 0.1250; 0 0.5 0];
scatter(JointEqBatERCOT(:,2),JointEqBatERCOT(:,1),JointEqBatERCOT(:,3)/1000, color, 'filled');
xlim([0 6]);
ylim([ErcotDL 200000]);
xlabel('Battery capacity in hours'); 
ylabel('Generation capacity in MW');
text(JointEqBatERCOT(:,2)*1.1,JointEqBatERCOT(:,1),{'$0', '$50', '$100', '$150'});
yl = yline(ErcotDH,'-',"Day Demand");
yl.Color = [.4 .4 .4];
fname = sprintf('EmissionEqERCOT.jpg');
%saveas(f, fname);%Print pictures

scatter(JointEqBatGER(:,2),JointEqBatGER(:,1),JointEqBatGER(:,3)/1000, color, 'filled');
xlim([0 6]);
ylim([GerDL 250000]);
xlabel('Battery capacity in hours'); 
ylabel('Generation capacity in MW');
text(JointEqBatGER(:,2)*1.1,JointEqBatGER(:,1),{'$0', '$50', '$100', '$150'});
yl = yline(GerDH,'-',"Day Demand");
yl.Color = [.4 .4 .4];
fname = sprintf('EmissionEqGER.jpg');
%saveas(f, fname);%Print pictures

scatter(JointEqBatPJM(:,2),JointEqBatPJM(:,1),JointEqBatPJM(:,3)/1000, color, 'filled');
xlim([0 6]);
ylim([PjmDL 300000]);
xlabel('Battery capacity in hours'); 
ylabel('Generation capacity in MW');
text(JointEqBatPJM(:,2)*1.1,JointEqBatPJM(:,1),{'$0', '$50', '$100', '$150'});
yl = yline(PjmDH,'-',"Day Demand");
yl.Color = [.4 .4 .4];
fname = sprintf('EmissionEqPJM.jpg');
%saveas(f, fname);%Print pictures

%Battery and Kraftblock
f = figure('visible', 'on');
scatter(JointEqBatERCOT(:,2),JointEqBatERCOT(:,1),JointEqBatERCOT(:,3)/1000, 'b', 'filled');
hold on
scatter(JointEqKraftERCOT(:,2),JointEqKraftERCOT(:,1),JointEqKraftERCOT(:,3)/1000, [.8 .5 .1], 'filled');
xlim([0 6]);
ylim([ErcotDL 225000]);
xlabel('Storage capacity in hours'); 
ylabel('Generation capacity in MW');
text(JointEqBatERCOT(:,2)*1.1,JointEqBatERCOT(:,1),{'$0', '$50', '$100', '$150'});
text(JointEqKraftERCOT(:,2)*1.1,JointEqKraftERCOT(:,1),{'$0', '$50', '$100', '$150'});
yl = yline(ErcotDH,'-',"Day Demand");
yl.Color = [.4 .4 .4];
legend({'Battery', 'Thermal'},'Location','northwest')
fname = sprintf('StorageTechEqERCOT.jpg');
%saveas(f, fname);%Print pictures
hold off

f = figure('visible', 'on');
scatter(JointEqBatGER(:,2),JointEqBatGER(:,1),JointEqBatGER(:,3)/1000, 'b', 'filled');
hold on
scatter(JointEqKraftGER(:,2),JointEqKraftGER(:,1),JointEqKraftGER(:,3)/1000, [.8 .5 .1], 'filled');
xlim([0 6]);
ylim([GerDL 300000]);
xlabel('Storage capacity in hours'); 
ylabel('Generation capacity in MW');
text(JointEqBatGER(:,2)*1.1,JointEqBatGER(:,1),{'$0', '$50', '$100', '$150'});
text(JointEqKraftGER(:,2)*1.1,JointEqKraftGER(:,1),{'$0', '$50', '$100', '$150'});
yl = yline(GerDH,'-',"Day Demand");
yl.Color = [.4 .4 .4];
legend({'Battery', 'Thermal'},'Location','northwest')
fname = sprintf('StorageTechEqGER.jpg');
%saveas(f, fname);%Print pictures
hold off

f = figure('visible', 'on');
scatter(JointEqBatPJM(:,2),JointEqBatPJM(:,1),JointEqBatPJM(:,3)/1000, 'b', 'filled');
hold on
scatter(JointEqKraftPJM(:,2),JointEqKraftPJM(:,1),JointEqKraftPJM(:,3)/1000, [.8 .5 .1], 'filled');
xlim([0 6]);
ylim([PjmDL 350000]);
xlabel('Storage capacity in hours'); 
ylabel('Generation capacity in MW');
text(JointEqBatPJM(:,2)*1.1,JointEqBatPJM(:,1),{'$0', '$50', '$100', '$150'});
text(JointEqKraftPJM(:,2)*1.1,JointEqKraftPJM(:,1),{'$0', '$50', '$100', '$150'});
yl = yline(PjmDH,'-',"Day Demand");
yl.Color = [.4 .4 .4];
legend({'Battery', 'Thermal'},'Location','northwest')
fname = sprintf('StorageTechEqPJM.jpg');
%saveas(f, fname);%Print pictures
hold off

%Do battery technology graph at t = 0
jt3IntersectRes0 = load('Results/ERCOT_Battery_Comp/BatteryCompResults_t=0.mat')
jt3IntersectRes0 = jt3IntersectRes0.('jt3IntersectRes0')

%3D Graph prep
effOptions = unique(jt3IntersectRes0(:,3))';
priceOptions = unique(jt3IntersectRes0(:,4))';
colcounter = 0;
allcounter = 0;
batTechArray = [];

for c = priceOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = effOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        batTechArray(rowcounter,colcounter) = jt3IntersectRes0(allcounter,5);
    end
end

[X,Y] = meshgrid(effOptions, priceOptions);
surf(X,Y,batTechArray');
xlabel('Storage efficiency');
ylabel('Storage cost per MWh per day in $'); 
zlabel('Average system profit');

%Do battery technology graph at t = 50
jt3IntersectRes50 = load('Results/ERCOT_Battery_Comp/BatteryCompResults_t=50.mat')
jt3IntersectRes50 = jt3IntersectRes50.('jt3IntersectRes50')

%3D Graph prep
effOptions50 = unique(jt3IntersectRes50(:,3))';
priceOptions50 = unique(jt3IntersectRes50(:,4))';
colcounter = 0;
allcounter = 0;
batTechArray50 = []

for c = priceOptions50
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = effOptions50
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        batTechArray50(rowcounter,colcounter) = jt3IntersectRes50(allcounter,5);
    end
end

[X,Y] = meshgrid(effOptions50, priceOptions50);
surf(X,Y,batTechArray50');
xlabel('Storage efficiency');
ylabel('Storage cost per MWh per day in $'); 
zlabel('Average system profit');

%Do battery technology graph at t = 100
jt3IntersectRes100 = load('Results/ERCOT_Battery_Comp/BatteryCompResults_t=100.mat')
jt3IntersectRes100 = jt3IntersectRes100.('jt3IntersectRes100')

%3D Graph prep
effOptions100 = unique(jt3IntersectRes100(:,3))';
priceOptions100 = unique(jt3IntersectRes100(:,4))';
colcounter = 0;
allcounter = 0;
batTechArray100 = []

for c = priceOptions100
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = effOptions100
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        batTechArray100(rowcounter,colcounter) = jt3IntersectRes100(allcounter,5);
    end
end

[X,Y] = meshgrid(effOptions100, priceOptions100);
surf(X,Y,batTechArray100');
xlabel('Storage efficiency');
ylabel('Storage cost per MWh per day in $'); 
zlabel('Average system profit');

%Do cheaper graph for gen and storage for ERCOT Battery
jt3IntersectRes = load('Results/Price_Comp/TechCheaper_ERCOT_Bat.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperERCOT_Bat')

batOptions = unique(jt3IntersectRes(:,3))';
genOptions = unique(jt3IntersectRes(:,4))';
colcounter = 0;
allcounter = 0;
cheapTechArray = []

for c = genOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = batOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        cheapTechArray(rowcounter,colcounter) = jt3IntersectRes(jt3IntersectRes(:,3)==r &jt3IntersectRes(:,4)==c,5);
    end
end

[X,Y] = meshgrid(batOptions, genOptions);
surf(X,Y,cheapTechArray');
xlabel('Battery Storage Cost in $ per MWh');
ylabel('Generation Cost in $ per MWh'); 
zlabel('Average system profit');

%Do cheaper graph for gen and storage for ERCOT Thermal
jt3IntersectRes = load('Results/Price_Comp/TechCheaper_ERCOT_Thermal.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperERCOT_Thermal')

batOptions = unique(jt3IntersectRes(:,3))';
genOptions = unique(jt3IntersectRes(:,4))';
colcounter = 0;
allcounter = 0;
cheapTechArray = []

for c = genOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = batOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        try
            cheapTechArray(rowcounter,colcounter) = jt3IntersectRes(jt3IntersectRes(:,3)==r &jt3IntersectRes(:,4)==c,5);
        except
            cheapTechArray(rowcounter,colcounter) = -5000000000
        end
    end
    
end

[X,Y] = meshgrid(batOptions, genOptions);
surf(X,Y,cheapTechArray');
hold on
mesh(X,Y,zeros(size(X, 1)))
hold off
xlabel('Thermal Storage Cost in $ per MWh');
ylabel('Generation Cost in $ per MWh'); 
zlabel('Average system profit');

%Do cheaper graph for gen and storage for PJM Battery
jt3IntersectRes = load('Results/Price_Comp/TechCheaper_PJM_Bat2.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperPJM_Bat')

batOptions = unique(jt3IntersectRes(:,3))';
genOptions = unique(jt3IntersectRes(:,4))';
colcounter = 0;
allcounter = 0;
cheapTechArray = []

for c = genOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = batOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        try
            cheapTechArray(rowcounter,colcounter) = jt3IntersectRes(jt3IntersectRes(:,3)==r &jt3IntersectRes(:,4)==c,5);
        except
            cheapTechArray(rowcounter,colcounter) = -5000000000
        end
    end
end

[X,Y] = meshgrid(batOptions, genOptions);
surf(X,Y,cheapTechArray');
xlabel('Battery Storage Cost in $ per MWh');
ylabel('Generation Cost in $ per MWh'); 
zlabel('Average system profit');

%Do cheaper graph for gen and storage for PJM Thermal
jt3IntersectRes = load('Results/Price_Comp/TechCheaper_PJM_Thermal2.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperPJM_Thermal')

batOptions = unique(jt3IntersectRes(:,3))';
genOptions = unique(jt3IntersectRes(:,4))';
colcounter = 0;
allcounter = 0;
cheapTechArray = []

for c = genOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = batOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        try
            cheapTechArray(rowcounter,colcounter) = jt3IntersectRes(jt3IntersectRes(:,3)==r &jt3IntersectRes(:,4)==c,5);
        except
            cheapTechArray(rowcounter,colcounter) = -5000000000
        end
    end
end

[X,Y] = meshgrid(batOptions, genOptions);
surf(X,Y,cheapTechArray');
xlabel('Battery Storage Cost in $ per MWh');
ylabel('Generation Cost in $ per MWh'); 
zlabel('Average system profit');


%Do cheaper graph for gen and storage for GER Battery
jt3IntersectRes = load('Results/Price_Comp/TechCheaper_GER_Bat2.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperGER_Bat')

batOptions = unique(jt3IntersectRes(:,3))';
genOptions = unique(jt3IntersectRes(:,4))';
colcounter = 0;
allcounter = 0;
cheapTechArray = []

for c = genOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = batOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        try
            cheapTechArray(rowcounter,colcounter) = jt3IntersectRes(jt3IntersectRes(:,3)==r &jt3IntersectRes(:,4)==c,5);
        except
            cheapTechArray(rowcounter,colcounter) = -5000000000
        end
    end
end

[X,Y] = meshgrid(batOptions, genOptions);
surf(X,Y,cheapTechArray');
xlabel('Battery Storage Cost in $ per MWh');
ylabel('Generation Cost in $ per MWh'); 
zlabel('Average system profit');

%Do cheaper graph for gen and storage for GER Thermal
jt3IntersectRes = load('Results/Price_Comp/TechCheaper_GER_Thermal2.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperGER_Thermal')

batOptions = unique(jt3IntersectRes(:,3))';
genOptions = unique(jt3IntersectRes(:,4))';
colcounter = 0;
allcounter = 0;
cheapTechArray = []

for c = genOptions
    rowcounter = 0;
    colcounter = colcounter + 1;
    for r = batOptions
        rowcounter = rowcounter + 1;
        allcounter = allcounter +1;
        try
            cheapTechArray(rowcounter,colcounter) = jt3IntersectRes(jt3IntersectRes(:,3)==r &jt3IntersectRes(:,4)==c,5);
        except
            cheapTechArray(rowcounter,colcounter) = -5000000000
        end
    end
end

[X,Y] = meshgrid(batOptions, genOptions);
surf(X,Y,cheapTechArray');
xlabel('Thermal Storage Cost in $ per MWh');
ylabel('Generation Cost in $ per MWh'); 
zlabel('Average system profit');

%Intermittency test ERCOT mean + var
jt3IntersectRes = load('Results/Intermittency/IntermittencyERCOT_Mean_Var.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperERCOT')

s1 = scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,3),[],"blue"); hold on;

%ax1 = gca; hold on;
%ax1_pos = ax1.Position; hold on;
%ax2 = axes('Position',ax1_pos,'XAxisLocation','top','YAxisLocation','right','Color','none');
%hold on
%s2 = scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,1),[],"b",'Parent',ax2); hold on;
%lgnd = legend([s1, s2], {"Storage Size in Hours","Total storage in Mwh"},'Location','northwest');
%set(lgnd, 'Box', 'off');
xlabel('Intermittency in %');
ylabel('Storage Size in MWh'); 

%Intermittency test PJM mean + var
jt3IntersectRes = load('Results/Intermittency/IntermittencyPJM_Mean_Var.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperPJM')

scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,3))
xlabel('Intermittency in %');
ylabel('Storage Size in MWh'); 

%Intermittency test GER mean + var
jt3IntersectRes = load('Results/Intermittency/IntermittencyGER_Mean_Var.mat')
jt3IntersectRes = jt3IntersectRes.('TechCheaperGER')

scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,3))
xlabel('Intermittency in %');
ylabel('Storage Size in MWh'); 

%Intermittency test ERCOT mean only
jt3IntersectRes = load('Results/Intermittency/IntermittencyERCOT_Mean.mat')
jt3IntersectRes = jt3IntersectRes.('IntermittencyERCOT')

scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,3))
xlabel('Intermittency in %');
ylabel('Storage Size in MWh'); 

%Intermittency test PJM mean only
jt3IntersectRes = load('Results/Intermittency/IntermittencyPJM_Mean.mat')
jt3IntersectRes = jt3IntersectRes.('IntermittencyPJM')

scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,3))
xlabel('Intermittency in %');
ylabel('Storage Size in MWh'); 

%Intermittency test GER mean only
jt3IntersectRes = load('Results/Intermittency/IntermittencyGER_Mean.mat')
jt3IntersectRes = jt3IntersectRes.('IntermittencyGER')

scatter(jt3IntersectRes(:,4),jt3IntersectRes(:,3))
xlabel('Intermittency in %');
ylabel('Storage Size in MWh'); 
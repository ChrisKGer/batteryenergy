clear all
tic

test = []
global r e nDay nNight muH muL sigH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim batSeq genSeq cS100Perc cG100Perc
r = 0.3;
nDay = 14;
nNight = 24- nDay;
muH = nDay * r;
muL = nNight * r;
sigH = sqrt(nDay * r * (1-r));
sigL = sqrt(nNight * r * (1-r));
QLim = fix(((DH+DL)/r - DH)/1000)*1000;
g = 50;
cG = 111;
pH = 37;
pL = 23;
DH = 65000;
DL = 35000;
genSeq2 = [36000 40000 45000 50000 55000 60000 65000];
genSeq = [66000 68000 70000 75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000 300000];
batSeq = [0 0.25 0.5 0.75 1 1.5 2 2.5 3 3.5 4 5 6 7 8 10 12 14];
pEmmit = 0.73 %0.73 tonnes, for the average MWh being produced, based on ERCOT plant emission data
tax = 0;
%Batter stuff
e = 0.9; %0.9 for battery %0.45 for Kraftblock
cS = 60; %60 for Battery %9 for Kraftblock
cS100Perc = cS
cG100Perc = cG

%Select Correct Simmulation Market
market = "GER" %TEX, GER, PJM
if market == "GER"
    pH = 58;
    pL = 46;
    DH = 72000;
    DL = 48000;
    g = 75;
    genSeq2 = [49000 55000 60000 65000 70000 72000];
    genSeq = [75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000 275000];
    pEmmit = 0.62 
elseif market == "PJM"
    pH = 25;
    pL = 19;
    DH = 99000;
    DL = 84000;
    g = 38;
    genSeq2 = [85000 87500 90000 92500 95000 99000];
    genSeq = [99000 100000 105000 110000 120000 130000 150000 175000 200000 225000 250000 275000 300000 325000 350000];
    pEmmit = 0.51
end    

gEmmit = 0.45; %0.45 tonnes, for a basic gas back-up plant, based on ERCOT plant emission data
g = g + gEmmit * tax;
pL =  min(pL + pEmmit * tax,g);
pH = min(pH + pEmmit * tax, g);
        
%Create integrals of specific normal cdfs
bH = @(x) exp(-((x-muH).^2)/(2*sigH.^2)) / (sigH*sqrt(2*pi));
BH = @(y) integral(@(x) bH(x), 0, y);
IntBH = @(z) integral(@(y) BH(y), 0, z, 'ArrayValued',true);
bL = @(x) exp(-((x-muL).^2)/(2*sigL.^2)) / (sigL*sqrt(2*pi));
BL = @(y) integral(@(x) bL(x), 0, y);
IntBL = @(z) integral(@(y) BL(y), 0, z, 'ArrayValued',true);

%StorArrCost = [5 10 15 20 25 30 40 50 60 80];
%StorArrEff = [.10 .25 .40 .45 .5 .55 .60 .65 .70 .75 .80 .85 .90 .95 1];
genCost = [0.05 : 0.05 : 1];
batCost = [0.05 : 0.05 : 1];
bruteForceCounter = 0

for genPerc = genCost
    for batPerc = batCost
        disp(['genPerc: ',num2str(genPerc),' & batPerc: ',num2str(batPerc)])
        
        %Set reduced prices for generation and Storage
        cG = cG100Perc * genPerc;
        cS = cS100Perc * batPerc;
        
        counter = 0;
        for i = batSeq
            counter = counter + 1;
            fixed_K = i;
            jt3PartialQFixK = @(Q) jt3PartialQ(Q, fixed_K)^2;
            try
                options = optimoptions('fmincon','Display','off');
                [x,fval,exitflag,output] = fmincon(jt3PartialQFixK, DH+1000, [], [], [], [] ,DH+1, (DH+DL)/r-DH, [], options);
                jt3FixKRes(counter,:) = [i, x];
            catch
                disp([i,"None Found"])
            end
        end

        counter = 0;
        for i = genSeq
            counter = counter + 1;
            fixed_Q = i;
            jt3PartialKFixQ = @(K) jt3PartialK(fixed_Q, K)^2;
            try
                options = optimoptions('fmincon','Display','off');
                [x,fval,exitflag,output] = fmincon(jt3PartialKFixQ, 0, [], [], [], [] ,0, 14, [], options);
                jt3FixQRes(counter,:) = [x, i];
            catch
                disp([i,"None Found"])
            end
        end
        
        %Store numerical results
        bruteForceCounter = bruteForceCounter+1;
        [jt3IntersectRes(bruteForceCounter,1),jt3IntersectRes(bruteForceCounter,2)] = jt3Intersect(jt3FixKRes, jt3FixQRes);
        jt3IntersectRes(bruteForceCounter,3) = cS;
        jt3IntersectRes(bruteForceCounter,4) = cG;
        jt3IntersectRes(bruteForceCounter,5) = ObjFuncJT(jt3IntersectRes(bruteForceCounter,1),jt3IntersectRes(bruteForceCounter,2));
        toc
    end
end

%Save Intersection points
save('TeachCheaperResults','jt3IntersectRes')

%Define all more complicated functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Joint - Case 3 - Partial Q
function profit = jt3PartialQ(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = g*e*(K - IntBH(K*V) - K*BH(K*V)*(DL-DH)/(Q-DH)) - ...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (K*(2*Q-DH-DL)-2*(Q-DH) * IntBH(K*V)- K * BH(K*V)* (DL-DH)) + ...
        g*e*(K-IntBL(K))*(1-(sqrt(r)*(2*Q-DH-DL))/((DL+DH)/r-DH)) -...
        cG - K*cS;
end

%Joint - Case 3 - Partial K
function profit = jt3PartialK(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = min((Q-DL)/(Q-DH),100);
    profit = g*e*(1-BH(K*V)) - ...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BH(K*V)) + ...
        g*e*(1-BL(K)) -...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BL(K)) - ...
        cS;
end

function profit = ObjFuncJT(K, Q)
    global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = min((Q-DL)/(Q-DH),100); %Avoid infinity integrals
        
    profit1 = nNight*(pL*DL-(1-r)*g*DL) ;
    profit2 = (g*e)*(Q-DH)*(K*V - IntBH(K*V))*(1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH))) ;
    profit3 = nDay * (pH*DH-(1-r)*g*DH);
    profit4 = (g*e)*(Q-DL)*(K - IntBL(K))*(1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH)));
    profit5 = -cG * Q - cS * K * (Q-DL);
    profit = profit1 + profit2 + profit3 + profit4 + profit5;
end

  
%Intersects2
function [optiK, optiQ] = jt3Intersect(arr1, arr2)
    global batSeq genSeq DH
    %Set default values
    optiK = 0;
    optiQ = DH;
    
    %Run function to test whether there are any intersecting line segments
    test = lineSegmentIntersect([arr1(1:length(batSeq)-1,:), arr1(2:length(batSeq),:)],...
        [arr2(1:length(genSeq)-1,:), arr2(2:length(genSeq),:)]);
    
    %If there are, store its values
    if sum(sum(test.("intAdjacencyMatrix"))) == 1
        optiK = test.("intMatrixX")(test.("intAdjacencyMatrix"));
        optiQ = test.("intMatrixY")(test.("intAdjacencyMatrix"));
    end
end

%Interscetion Function
function out = lineSegmentIntersect(XY1,XY2)

    % Author:  U. Murat Erdem
    validateattributes(XY1,{'numeric'},{'2d','finite'});
    validateattributes(XY2,{'numeric'},{'2d','finite'});
    [n_rows_1,n_cols_1] = size(XY1);
    [n_rows_2,n_cols_2] = size(XY2);
    if n_cols_1 ~= 4 || n_cols_2 ~= 4
        error('Arguments must be a Nx4 matrices.')
    end
    %%% Prepare matrices for vectorized computation of line intersection points.
    %-------------------------------------------------------------------------------
    X1 = repmat(XY1(:,1),1,n_rows_2);
    X2 = repmat(XY1(:,3),1,n_rows_2);
    Y1 = repmat(XY1(:,2),1,n_rows_2);
    Y2 = repmat(XY1(:,4),1,n_rows_2);
    XY2 = XY2';
    X3 = repmat(XY2(1,:),n_rows_1,1);
    X4 = repmat(XY2(3,:),n_rows_1,1);
    Y3 = repmat(XY2(2,:),n_rows_1,1);
    Y4 = repmat(XY2(4,:),n_rows_1,1);
    X4_X3 = (X4-X3);
    Y1_Y3 = (Y1-Y3);
    Y4_Y3 = (Y4-Y3);
    X1_X3 = (X1-X3);
    X2_X1 = (X2-X1);
    Y2_Y1 = (Y2-Y1);
    numerator_a = X4_X3 .* Y1_Y3 - Y4_Y3 .* X1_X3;
    numerator_b = X2_X1 .* Y1_Y3 - Y2_Y1 .* X1_X3;
    denominator = Y4_Y3 .* X2_X1 - X4_X3 .* Y2_Y1;
    u_a = numerator_a ./ denominator;
    u_b = numerator_b ./ denominator;
    % Find the adjacency matrix A of intersecting lines.
    INT_X = X1+X2_X1.*u_a;
    INT_Y = Y1+Y2_Y1.*u_a;
    INT_B = (u_a >= 0) & (u_a <= 1) & (u_b >= 0) & (u_b <= 1);
    PAR_B = denominator == 0;
    COINC_B = (numerator_a == 0 & numerator_b == 0 & PAR_B);
    % Arrange output.
    out.intAdjacencyMatrix = INT_B;
    out.intMatrixX = INT_X .* INT_B;
    out.intMatrixY = INT_Y .* INT_B;
    out.intNormalizedDistance1To2 = u_a;
    out.intNormalizedDistance2To1 = u_b;
    out.parAdjacencyMatrix = PAR_B;
    out.coincAdjacencyMatrix= COINC_B;
end
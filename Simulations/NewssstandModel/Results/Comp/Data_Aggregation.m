testfiledir = pwd;
testfiledir = join([testfiledir,'\GER_t100_Bat']);
matfiles = dir(fullfile(testfiledir, '*.mat'));
nfiles = length(matfiles);
Comp_GER_100_Bat = zeros(nfiles,6);
for i = 1 : nfiles
   disp(i)
   dummy = load(fullfile(testfiledir, matfiles(i).name));
   dummy = dummy.("cp3IntersectRes");
   Comp_GER_100_Bat(i,:) = dummy;
end
testfiledir = pwd;
testfiledir = join([testfiledir,'\GER2']);
matfiles = dir(fullfile(testfiledir, '*.mat'));
nfiles = length(matfiles);
TechCheaperGER_Bat = zeros(nfiles,5);
for i = 1 : nfiles
   disp(i)
   dummy = load(fullfile(testfiledir, matfiles(i).name));
   dummy = dummy.("jt3IntersectRes");
   TechCheaperGER_Bat(i,:) = dummy;
end
testfiledir = pwd;
testfiledir = join([testfiledir,'\PJM2']);
matfiles = dir(fullfile(testfiledir, '*.mat'));
nfiles = length(matfiles);
TechCheaperPJM_Bat = zeros(nfiles,5);
for i = 1 : nfiles
   disp(i)
   dummy = load(fullfile(testfiledir, matfiles(i).name));
   dummy = dummy.("jt3IntersectRes");
   TechCheaperPJM_Bat(i,:) = dummy;
end
testfiledir = pwd;
matfiles = dir(fullfile(testfiledir, '*.mat'));
nfiles = length(matfiles);
TechCheaperPJM = zeros(nfiles,5);
for i = 1 : nfiles
   disp(i)
   dummy = load(fullfile(testfiledir, matfiles(i).name));
   dummy = dummy.("jt3IntersectRes");
   disp(dummy);
   size(dummy);
   TechCheaperPJM(i,:) = dummy;
end
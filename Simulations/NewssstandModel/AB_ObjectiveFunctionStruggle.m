global r e nDay nNight muH muL sigH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim batSeq genSeq
r = 0.3;
nDay = 14;
nNight = 24- nDay;
muH = nDay * r;
muL = nNight * r;
sigH = sqrt(nDay * r * (1-r));
sigL = sqrt(nNight * r * (1-r));
QLim = fix(((DH+DL)/r - DH)/1000)*1000;
g = 50;
cG = 11.1;
pH = 37;
pL = 23;
DH = 65000;
DL = 35000;
genSeq2 = [36000 40000 45000 50000 55000 60000 65000];
genSeq = [66000:5000:350000];
batSeq = [0:0.25:14];
pEmmit = 0.73 %0.73 tonnes, for the average MWh being produced, based on ERCOT plant emission data
tax = 0;
%Batter stuff
e = 0.9; %0.9 for battery %0.45 for Kraftblock
cS = 30; %60 for Battery %9 for Kraftblock

%Create integrals of specific normal cdfs
bH = @(x) exp(-((x-muH).^2)/(2*sigH.^2)) / (sigH*sqrt(2*pi));
BH = @(y) integral(@(x) bH(x), 0, y);
IntBH = @(z) integral(@(y) BH(y), 0, z, 'ArrayValued',true);
bL = @(x) exp(-((x-muL).^2)/(2*sigL.^2)) / (sigL*sqrt(2*pi));
BL = @(y) integral(@(x) bL(x), 0, y);
IntBL = @(z) integral(@(y) BL(y), 0, z, 'ArrayValued',true);

%Download Intersections
jt3IntersectRes0 = load('Results/ERCOT_Battery_Comp/BatteryCompResults_t=0.mat')
jt3IntersectRes0 = jt3IntersectRes0.('jt3IntersectRes0')
[numObs, ~] =  size(jt3IntersectRes0)

%Select Correct Simmulation Market
market = "TEX" %TEX, GER, PJM
if market == "GER"
    pH = 58;
    pL = 46;
    DH = 72000;
    DL = 48000;
    g = 75;
    genSeq2 = [49000 55000 60000 65000 70000 72000];
    genSeq = [75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000 275000];
    pEmmit = 0.62 
elseif market == "PJM"
    pH = 25;
    pL = 19;
    DH = 99000;
    DL = 84000;
    g = 38;
    genSeq2 = [85000 87500 90000 92500 95000 99000];
    genSeq = [99000 100000 105000 110000 120000 130000 150000 175000 200000 225000 250000 275000 300000 325000 350000];
    pEmmit = 0.51
end    

gEmmit = 0.45; %0.45 tonnes, for a basic gas back-up plant, based on ERCOT plant emission data
g = g + gEmmit * tax;
pL =  min(pL + pEmmit * tax,g);
pH = min(pH + pEmmit * tax, g);

for i = 1:numObs
    e = jt3IntersectRes0(i,3);
    cS = jt3IntersectRes0(i,4);

    jt3IntersectRes0(i,5) = ObjFuncJT( jt3IntersectRes0(i,1), jt3IntersectRes0(i,2));
end


colCount = 0;
for k = batSeq
    rowCount = 0;
    colCount = colCount+1
    for q = genSeq
        rowCount = rowCount +1;
        resArray(rowCount,colCount) = ObjFuncJT(k,q);
    end
end


%Define all more complicated functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function profit = ObjFuncJT(K, Q)
    global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = min((Q-DL)/(Q-DH),100); %Avoid infinity integrals
        
    profit1 = nNight*(pL*DL-(1-r)*g*DL) ;
    profit2 = (g*e)*(Q-DH)*(K*V - IntBH(K*V))*(1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH))) ;
    profit3 = nDay * (pH*DH-(1-r)*g*DH);
    profit4 = (g*e)*(Q-DL)*(K - IntBL(K))*(1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH)));
    profit5 = -cG * Q - cS * K * (Q-DL);
    profit = profit1 + profit2 + profit3 + profit4 + profit5;
end
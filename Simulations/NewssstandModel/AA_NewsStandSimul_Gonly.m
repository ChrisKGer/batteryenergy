clear all

global r e nDay nNight muH muL sigH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
r = 0.3;
nDay = 14;
nNight = 24- nDay;
muH = nDay * r;
muL = nNight * r;
sigH = sqrt(nDay * r * (1-r));
sigL = sqrt(nNight * r * (1-r));
QLim = fix(((DH+DL)/r - DH)/1000)*1000;
g = 50;
cG = 111;
pH = 37;
pL = 23;
DH = 65000;
DL = 35000;
genSeq2 = [36000 40000 45000 50000 55000 60000 65000];
genSeq = [66000 68000 70000 75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000];
pEmmit = 0.73 %0.73 tonnes, for the average MWh being produced, based on ERCOT plant emission data
tax = 150;
%Batter stuff
e = 0.45; %0.9 for battery %0.45 for Kraftblock
cS = 9; %60 for Battery %9 for Kraftblock
cS = cS + 0.005*tax %add emission tax (0.005 for Kraftblock)

%Select Correct Simmulation Market
market = "TEX" %TEX, GER, PJM
if market == "GER"
    pH = 58;
    pL = 46;
    DH = 72000;
    DL = 48000;
    g = 75;
    genSeq2 = [49000 55000 60000 65000 70000 72000];
    genSeq = [75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000 275000];
    pEmmit = 0.62 
elseif market == "PJM"
    pH = 25;
    pL = 19;
    DH = 99000;
    DL = 84000;
    g = 38;
    genSeq2 = [85000 87500 90000 92500 95000 99000];
    genSeq = [99000 100000 105000 110000 120000 130000 150000 175000 200000 225000 250000 275000 300000 325000 350000];
    pEmmit = 0.51
end    

%Account for emissions
gEmmit = 0.45 %0.45 tonnes, for a basic gas back-up plant, based on ERCOT plant emission data
g = g + gEmmit * tax
pL =  min(pL + pEmmit * tax,g)
pH = min(pH + pEmmit * tax, g)


%Create integrals of specific normal cdfs
bH = @(x) exp(-((x-muH).^2)/(2*sigH.^2)) / (sigH*sqrt(2*pi));
BH = @(y) integral(@(x) bH(x), 0, y);
IntBH = @(z) integral(@(y) BH(y), 0, z, 'ArrayValued',true);
bL = @(x) exp(-((x-muL).^2)/(2*sigL.^2)) / (sigL*sqrt(2*pi));
BL = @(y) integral(@(x) bL(x), 0, y);
IntBL = @(z) integral(@(y) BL(y), 0, z, 'ArrayValued',true);

%Cross-Derivative%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Case 2

%Just calculated all possible positions
rowcounter = 0;
colcounter = 0;
batSeq = [0 0.25 0.5 0.75 1 1.5 2 2.5 3 3.5 4 5 6 7 8 10];
for k = batSeq
    rowcounter = 0;
    colcounter = colcounter + 1;
    disp(['XDerivGrid K= ',num2str(k)])
    for q = genSeq2
        rowcounter = rowcounter + 1;
        x = c2crossDeriv(q,k);
        crossDerivArray2(rowcounter,colcounter) = x;
    end
end

[X2,Y2] = meshgrid(batSeq, genSeq2);
surf(X2,Y2,crossDerivArray2)
contour3(X2,Y2,crossDerivArray2, [0,0])%Print contour of where derivative is 0


%Case 3
%Just calculated all possible positions
rowcounter = 0;
colcounter = 0;

for k = batSeq
    rowcounter = 0;
    colcounter = colcounter + 1;
    disp(['XDerivGrid K= ',num2str(k)])
    for q = genSeq
        rowcounter = rowcounter + 1;
        x = c3crossDeriv(q,k);
        crossDerivArray3(rowcounter,colcounter) = x;
    end
end

[X3,Y3] = meshgrid(batSeq, genSeq);
surf(X3,Y3,crossDerivArray3);
contour3(X3,Y3,crossDerivArray3, [0,0]); %Print contour of where derivative is 0
xlabel('Battery capacity in hours'); 
ylabel('Generation capacity in MW');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Joint Optimum
jt3PartialQ(80000, 2); %Randomly just run on your own choice of variables

%Optimal Response Functions for Joint Cased
%Fix K
fixed_K = 2;
jt3PartialQFixK = @(Q) jt3PartialQ(Q, fixed_K);

counter = 0;
for i = batSeq
    counter = counter + 1;
    fixed_K = i;
    disp(['JT Case3 FixK ',num2str(fixed_K)])
    jt3PartialQFixK = @(Q) jt3PartialQ(Q, fixed_K)^2;
    try
        options = optimoptions('fmincon','Display','off');
        [x,fval,exitflag,output] = fmincon(jt3PartialQFixK, DH+1000, [], [], [], [] ,DH+1, (DH+DL)/r-DH, [], options);
        jt3FixKRes(counter,:) = [i, x];
    catch
        disp([i,"None Found"])
    end
end
plot(jt3FixKRes(:,1),jt3FixKRes(:,2))
hold on %Waits to close grpah until the other response curve is drawn

%Fix Q
fixed_Q = 80000;
jt3PartialKFixQ = @(K) jt3PartialK(fixed_Q, K);

counter = 0;
for i = genSeq
    counter = counter + 1;
    fixed_Q = i;
    disp(['JT Case3 FixQ ',num2str(fixed_Q)])
    jt3PartialKFixQ = @(K) jt3PartialK(fixed_Q, K)^2;
    try
        options = optimoptions('fmincon','Display','off');
        [x,fval,exitflag,output] = fmincon(jt3PartialKFixQ, 0, [], [], [], [] ,0, 6, [], options);
        jt3FixQRes(counter,:) = [x, i];
    catch
        disp([i,"None Found"])
    end
end
plot(jt3FixQRes(:,1),jt3FixQRes(:,2))
xlabel('Battery capacity in hours'); 
ylabel('Generation capacity in MW');
legend("Storage set, pick optimal generation", "Generation set, pick optimal storage")
hold off

%Define all more complicated functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Define cross derivative function
function profit = cAllcrossDeriv(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
    V = (Q-DL)/(Q-DH);
  
    if Q <= DL
        profit = 0;
    elseif Q <= DH
        profit = g*e* (1-BL(K)) * (1-( sqrt(r)*(2*Q-DH-DL))/((DL+DH)/r - DH))-...
        cS;
    else 
        profit = g*e*(1-BH(K*V) + K*V*bH(K*V)*(DH-DL)/(Q-DH)) - ...
        ((sqrt(r)*g*e)/((DL+DH)/r - DH)) * ((2*Q-DH-DL)*(1-BH(K*V))+ K*V*bH(K*V)*(DH-DL)) + ...
        g*e*(1-BL(K)) -...
        ((sqrt(r)*g*e)/((DL+DH)/r - DH))*(2*Q-DH-DL)*(1-BL(K))-...
        cS;
    end
end


function profit = c2crossDeriv(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
    V = (Q-DL)/(Q-DH);  
    profit = g*e* (1-BL(K)) * (1-( sqrt(r)*(2*Q-DH-DL))/((DL+DH)/r - DH))-...
        cS;
end

function profit = c3crossDeriv(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
    V = (Q-DL)/(Q-DH);
    profit = g*e*(1-BH(K*V) + K*V*bH(K*V)*(DH-DL)/(Q-DH)) - ...
        ((sqrt(r)*g*e)/((DL+DH)/r - DH)) * ((2*Q-DH-DL)*(1-BH(K*V))+ K*V*bH(K*V)*(DH-DL)) + ...
        g*e*(1-BL(K)) -...
        ((sqrt(r)*g*e)/((DL+DH)/r - DH))*(2*Q-DH-DL)*(1-BL(K))-...
        cS;
end

%Joint - Case 3 - Partial Q
function profit = jt3PartialQ(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = g*e*(K - IntBH(K*V) - K*BH(K*V)*(DL-DH)/(Q-DH)) - ...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (K*(2*Q-DH-DL)-2*(Q-DH) * IntBH(K*V)- K * BH(K*V)* (DL-DH)) + ...
        g*e*(K-IntBL(K))*(1-(sqrt(r)*(2*Q-DH-DL))/((DL+DH)/r-DH)) -...
        cG - K*cS;
end

%Joint - Case 3 - Partial K
function profit = jt3PartialK(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = g*e*(1-BH(K*V)) - ...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BH(K*V)) + ...
        g*e*(1-BL(K)) -...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BL(K)) - ...
        cS;
end

%Intersection Function
function out = lineSegmentIntersect(XY1,XY2)
    % Author:  U. Murat Erdem
    validateattributes(XY1,{'numeric'},{'2d','finite'});
    validateattributes(XY2,{'numeric'},{'2d','finite'});
    [n_rows_1,n_cols_1] = size(XY1);
    [n_rows_2,n_cols_2] = size(XY2);
    if n_cols_1 ~= 4 || n_cols_2 ~= 4
        error('Arguments must be a Nx4 matrices.');
    end
    %%% Prepare matrices for vectorized computation of line intersection points.
    %-------------------------------------------------------------------------------
    X1 = repmat(XY1(:,1),1,n_rows_2);
    X2 = repmat(XY1(:,3),1,n_rows_2);
    Y1 = repmat(XY1(:,2),1,n_rows_2);
    Y2 = repmat(XY1(:,4),1,n_rows_2);
    XY2 = XY2';
    X3 = repmat(XY2(1,:),n_rows_1,1);
    X4 = repmat(XY2(3,:),n_rows_1,1);
    Y3 = repmat(XY2(2,:),n_rows_1,1);
    Y4 = repmat(XY2(4,:),n_rows_1,1);
    X4_X3 = (X4-X3);
    Y1_Y3 = (Y1-Y3);
    Y4_Y3 = (Y4-Y3);
    X1_X3 = (X1-X3);
    X2_X1 = (X2-X1);
    Y2_Y1 = (Y2-Y1);
    numerator_a = X4_X3 .* Y1_Y3 - Y4_Y3 .* X1_X3;
    numerator_b = X2_X1 .* Y1_Y3 - Y2_Y1 .* X1_X3;
    denominator = Y4_Y3 .* X2_X1 - X4_X3 .* Y2_Y1;
    u_a = numerator_a ./ denominator;
    u_b = numerator_b ./ denominator;
    % Find the adjacency matrix A of intersecting lines.
    INT_X = X1+X2_X1.*u_a;
    INT_Y = Y1+Y2_Y1.*u_a;
    INT_B = (u_a >= 0) & (u_a <= 1) & (u_b >= 0) & (u_b <= 1);
    PAR_B = denominator == 0;
    COINC_B = (numerator_a == 0 & numerator_b == 0 & PAR_B);
    % Arrange output.
    out.intAdjacencyMatrix = INT_B;
    out.intMatrixX = INT_X .* INT_B;
    out.intMatrixY = INT_Y .* INT_B;
    out.intNormalizedDistance1To2 = u_a;
    out.intNormalizedDistance2To1 = u_b;
    out.parAdjacencyMatrix = PAR_B;
    out.coincAdjacencyMatrix= COINC_B;
end
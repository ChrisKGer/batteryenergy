clear all

global r e nDay nNight muH muL sigH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
r = 0.3;
e = 0.9; %0.9 for battery %0.45 for Kraftblock
nDay = 14;
nNight = 24- nDay;
muH = nDay * r;
muL = nNight * r;
sigH = sqrt(nDay * r * (1-r));
sigL = sqrt(nNight * r * (1-r));
DH = 65000;
DL = 35000;
QLim = fix(((DH+DL)/r - DH)/1000)*1000;
pH = 37;
pL = 23;
g = 50;
cS = 60; %60 for Battery %9 for Kraftblock
cG = 111;

%Account for emissions
tax = 100;
gEmmit = 0.45 %0.45 tonnes, for a basic gas back-up plant, based on ERCOT plant emission data
pEmmit = 0.73 %0.73 tonnes, for the average MWh being produced, based on ERCOT plant emission data
g = g + gEmmit * tax
pL =  min(pL + pEmmit * tax,g)
pH = min(pH + pEmmit * tax, g)

%Create integrals of specific normal cdfs
bH = @(x) exp(-((x-muH).^2)/(2*sigH.^2)) / (sigH*sqrt(2*pi));
BH = @(y) integral(@(x) bH(x), 0, y);
IntBH = @(z) integral(@(y) BH(y), 0, z, 'ArrayValued',true);
bL = @(x) exp(-((x-muL).^2)/(2*sigL.^2)) / (sigL*sqrt(2*pi));
BL = @(y) integral(@(x) bL(x), 0, y);
IntBL = @(z) integral(@(y) BL(y), 0, z, 'ArrayValued',true);

%Cross-Derivative%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Just calculated all possible positions
rowcounter = 0;
colcounter = 0;
batSeq = 0.0: 0.5: 6;
genSeq = [36000 40000 45000 50000 55000 60000 65000 68000 70000 75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000];
for k = batSeq
    rowcounter = 0;
    colcounter = colcounter + 1;
    disp(['XDerivGrid K= ',num2str(k)])
    for q = genSeq
        rowcounter = rowcounter + 1;
        x = cAllcrossDeriv(q,k);
        crossDerivArray(rowcounter,colcounter) = x;
    end
end

[X,Y] = meshgrid(batSeq, genSeq);
surf(X,Y,crossDerivArray)
contour3(X,Y,crossDerivArray, [0,0])%Print contour of where derivative is 0


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Joint Optimum
%Optimal Response Functions for Joint Cased
%Fix K
fixed_K = 2;
jtAllPartialQFixK = @(Q) jtAllPartialQ(Q, fixed_K);

counter = 0;
for i = batSeq
    counter = counter + 1;
    fixed_K = i;
    disp(['JT Case3 FixK ',num2str(fixed_K)])
    jtAllPartialQFixK = @(Q) jtAllPartialQ(Q, fixed_K)^2;
    try
        options = optimoptions('fmincon','Display','off');
        [x,fval,exitflag,output] = fmincon(jtAllPartialQFixK, DH+1000, [], [], [], [] ,0, (DH+DL)/r-DH, [], options);
        jtAllFixKRes(counter,:) = [i, x];
    catch
        disp([i,"None Found"])
    end
end
plot(jtAllFixKRes(:,1),jtAllFixKRes(:,2))
hold on %Waits to close graph until the other response curve is drawn

counter = 0;
for i = genSeq
    counter = counter + 1;
    fixed_Q = i;
    disp(['JT Case3 FixQ ',num2str(fixed_Q)])
    jtAllPartialKFixQ = @(K) jtAllPartialK(fixed_Q, K)^2;
    try
        options = optimoptions('fmincon','Display','off');
        [x,fval,exitflag,output] = fmincon(jtAllPartialKFixQ, 0, [], [], [], [] ,0, 6, [], options);
        jtAllFixQRes(counter,:) = [x, i];
    catch
        disp([i,"None Found"])
    end
end
plot(jtAllFixQRes(:,1),jtAllFixQRes(:,2))
hold off


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Define all more complicated functions
%Define cross derivative function
function profit = cAllcrossDeriv(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
    V = (Q-DL)/(Q-DH);
  
    if Q <= DL
        profit = 0;
    elseif Q <= DH
        profit = g*e* (1-BL(K))-...
        cS;
    else 
        profit = g*e*(1-BH(K*V) + K*V*bH(K*V)*(DH-DL)/(Q-DH)) - ...
        ((sqrt(r)*g*e)/((DL+DH)/r - DH)) * ((2*Q-DH-DL)*(1-BH(K*V))+ K*V*bH(K*V)*(DH-DL)) + ...
        g*e*(1-BL(K)) -...
        ((sqrt(r)*g*e)/((DL+DH)/r - DH))*(2*Q-DH-DL)*(1-BL(K))-...
        cS;
    end
end

%Joint - Case 3 - Partial Q
function profit = jtAllPartialQ(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
   V = (Q-DL)/(Q-DH);
   
   if Q <= DL
        profit = 24 *r*g - cG;
   elseif Q <= DH
        profit = nNight*r*g + g*e* (K - IntBL(K))-...
        cG - cS * K;
   else 
        profit = g*e*(K - IntBH(K*V) - K*BH(K*V)*(DL-DH)/(Q-DH)) - ...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (K*(2*Q-DH-DL)-2*(Q-DH) * IntBH(K*V)- K * BH(K*V)* (DL-DH)) + ...
        g*e*(K-IntBL(K))*(1-(sqrt(r)*(2*Q-DH-DL))/((DL+DH)/r-DH)) -...
        cG - K*cS;
   end
   
end

%Joint - Case 3 - Partial K
function profit = jtAllPartialK(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
   
    if Q <= DL
        profit = 0;
    elseif Q <= DH
        profit = g*e* (Q-DL) * (1 - BL(K)) -...
        (Q-DL) * cS;
    else   
        profit = g*e*(1-BH(K*V)) - ...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BH(K*V)) + ...
        g*e*(1-BL(K)) -...
        (g*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BL(K)) - ...
        cS;
    end
end
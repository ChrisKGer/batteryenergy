clear all

global r e nDay nNight muH muL sigH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
r = 0.3;
e = 0.45; %0.9 for battery %0.45 for Kraftblock
nDay = 14;
nNight = 24- nDay;
muH = nDay * r;
muL = nNight * r;
sigH = sqrt(nDay * r * (1-r));
sigL = sqrt(nNight * r * (1-r));
DH = 65000;
DL = 35000;
QLim = fix(((DH+DL)/r - DH)/1000)*1000;
pH = 37;
pL = 23;
g = 50;
cS = 9; %60 for Battery %9 for Kraftblock
cG = 111;

%Create integrals of specific normal cdfs
bH = @(x) exp(-((x-muH).^2)/(2*sigH.^2)) / (sigH*sqrt(2*pi));
BH = @(y) integral(@(x) bH(x), 0, y);
IntBH = @(z) integral(@(y) BH(y), 0, z, 'ArrayValued',true);
bL = @(x) exp(-((x-muL).^2)/(2*sigL.^2)) / (sigL*sqrt(2*pi));
BL = @(y) integral(@(x) bL(x), 0, y);
IntBL = @(z) integral(@(y) BL(y), 0, z, 'ArrayValued',true);

%Cross-Derivative%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Case 2
rowcounter = 0;
colcounter = 0;
batSeq = 0.0: 0.5: 6;
genSeq = [36000 40000 45000 50000 55000 60000 65000];
for k = batSeq
    rowcounter = 0;
    colcounter = colcounter + 1;
    disp(['XDerivGrid K= ',num2str(k)])
    for q = genSeq
        rowcounter = rowcounter + 1;
        x = c2crossDeriv(q,k);
        crossDerivArray(rowcounter,colcounter) = x;
    end
end

[X,Y] = meshgrid(batSeq, genSeq);
surf(X,Y,crossDerivArray)
contour3(X,Y,crossDerivArray, [0,0])%Print contour of where derivative is 0

%Case 3
%Just calculated all possible positions
rowcounter = 0;
colcounter = 0;
batSeq = 0.0: 0.5: 6;
genSeq = [68000 70000 75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000];
for k = batSeq
    rowcounter = 0;
    colcounter = colcounter + 1;
    disp(['XDerivGrid K= ',num2str(k)])
    for q = genSeq
        rowcounter = rowcounter + 1;
        x = c3crossDeriv(q,k);
        crossDerivArray(rowcounter,colcounter) = x;
    end
end

[X,Y] = meshgrid(batSeq, genSeq);
surf(X,Y,crossDerivArray)
contour3(X,Y,crossDerivArray, [0,0])%Print contour of where derivative is 0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Joint Optimum
%Optimal Response Functions for Joint Cased
%Fix K
fixed_K = 2;
jt3PartialQFixK = @(Q) jt3PartialQ(Q, fixed_K);

counter = 0;
for i = batSeq
    counter = counter + 1;
    fixed_K = i;
    disp(['JT Case3 FixK ',num2str(fixed_K)])
    jt3PartialQFixK = @(Q) jt3PartialQ(Q, fixed_K)^2;
    try
        options = optimoptions('fmincon','Display','off');
        [x,fval,exitflag,output] = fmincon(jt3PartialQFixK, DH+1000, [], [], [], [] ,DH+1, (DH+DL)/r-DH, [], options);
        jt3FixKRes(counter,:) = [i, x];
    catch
        disp([i,"None Found"])
    end
end
plot(jt3FixKRes(:,1),jt3FixKRes(:,2))
hold on %Waits to close grpah until the other response curve is drawn

%Fix Q
fixed_Q = 80000;
jt3PartialKFixQ = @(K) jt3PartialK(fixed_Q, K);

counter = 0;
for i = genSeq
    counter = counter + 1;
    fixed_Q = i;
    disp(['JT Case3 FixQ ',num2str(fixed_Q)])
    jt3PartialKFixQ = @(K) jt3PartialK(fixed_Q, K)^2;
    try
        options = optimoptions('fmincon','Display','off');
        [x,fval,exitflag,output] = fmincon(jt3PartialKFixQ, 0, [], [], [], [] ,0, 6, [], options);
        jt3FixQRes(counter,:) = [x, i];
    catch
        disp([i,"None Found"])
    end
end
plot(jt3FixQRes(:,1),jt3FixQRes(:,2))
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Competitive Setting
%Case 2
cp2(20)

%Define all more complicated functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Define cross derivative function
function profit = c2crossDeriv(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
    V = (Q-DL)/(Q-DH);
    profit = (pH+g)*e*(1-BL(K)) -...
        ((sqrt(r)*(pH+g)*e)/((DL+DH)/r - DH))*(2*Q-DH-DL)*(1-BL(K))-...
        cS;
end

function profit = c3crossDeriv(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim 
    V = (Q-DL)/(Q-DH);
    profit = (pL+g)*e*(1-BH(K*V) + K*V*bH(K*V)*(DH-DL)/(Q-DH)) - ...
        ((sqrt(r)*(pL+g)*e)/((DL+DH)/r - DH)) * ((2*Q-DH-DL)*(1-BH(K*V))+ K*V*bH(K*V)*(DH-DL)) + ...
        (pH+g)*e*(1-BL(K)) -...
        ((sqrt(r)*(pH+g)*e)/((DL+DH)/r - DH))*(2*Q-DH-DL)*(1-BL(K))-...
        cS;
end

%Joint - Case 3 - Partial Q
function profit = jt3PartialQ(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = (pL+g)*e*(K - IntBH(K*V) - K*BH(K*V)*(DL-DH)/(Q-DH)) - ...
        ((pL+g)*e*sqrt(r))/((DL+DH)/r - DH) * (K*(2*Q-DH-DL)-2*(Q-DH) * IntBH(K*V)- K * BH(K*V)* (DL-DH)) + ...
        (pH+g)*e*(K-IntBL(K))*(1-(sqrt(r)*(2*Q-DH-DL))/((DL+DH)/r-DH)) -...
        cG - K*cS;
end

%Joint - Case 3 - Partial K
function profit = jt3PartialK(Q,K)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = (pL+g)*e*(1-BH(K*V)) - ...
        ((pL+g)*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BH(K*V)) + ...
        (pH+g)*e*(1-BL(K)) -...
        ((pH+g)*e*sqrt(r))/((DL+DH)/r - DH) * (Q-DH)* (1-BL(K)) - ...
        cS;
end

%Comp - Case 2
function profit = cp2(tL)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    funDummy = @(k) ((pH - pL + tL) * (1-BL(k)) - cS)^2;
    options = optimoptions('fmincon','Display','off');
    [x,fval,exitflag,output] = fmincon(funDummy, 0, [], [], [], [] ,0, 6, [], options);
    
    derivVal = nDay*(r*pH+r*g) + (pL + g*e - tL) * (x - IntBL(x)) - cG
    if derivVal >=0
        profit = DH
    else
        profit = DL
    end

end

%Comp - Case 3 - Partial Q
function profit = cp3PartialQ(Q, K, tL, tH)
    global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = max(pH + g*e - tH,0) * (K - IntBH(K*V) - K*BH(K*V)*(DL-DH)/(Q-DH) - ...
        (sqrt(r)/((DL+DH)/r - DH)) * (K*(2*Q-DH-DL) - 2*(Q-DH)*IntBH(K*V) - K*BH(K*V)*(DL-DH))) + ...
        max(pL + g*e - tL,0) * (Q-DL) * (K-IntBL(K)) * (1 - ((sqrt(r) * (2*Q-DH-DL)) /((DL+DH)/r - DH))) - ...
        cG
end

%Comp - Case 3 - Partial K
function profit = cp3PartialK(Q, K, tL, tH)
    global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = (Q-DL)/(Q-DH);
    profit = max(pL*e - pH + tH,0) * (1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH))) * (1-BH(K*V))+ ...
        max(pH*e - pL + tL,0) * (1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH))) * (1-BL(K)) - ...
        cS
end
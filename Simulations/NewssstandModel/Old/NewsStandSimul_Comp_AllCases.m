clear all
tic

global r e nDay nNight muH muL sigH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim batSeq genSeq
r = 0.3;
e = 0.9; %0.9 for battery %0.45 for Kraftblock
nDay = 14;
nNight = 24- nDay;
muH = nDay * r;
muL = nNight * r;
sigH = sqrt(nDay * r * (1-r));
sigL = sqrt(nNight * r * (1-r));
DH = 65000;
DL = 35000;
QLim = fix(((DH+DL)/r - DH)/1000)*1000;
pH = 37;
pL = 23; 
g = 50;
cS = 60; %60 for Battery %9 for Kraftblock
cG = 111;

%Account for emissions
tax = 100;
gEmmit = 0.45; %0.45 tonnes, for a basic gas back-up plant, based on ERCOT plant emission data
pEmmit = 0.73; %0.73 tonnes, for the average MWh being produced, based on ERCOT plant emission data
g = g + gEmmit * tax;
pL = min(pL + pEmmit * tax, g)
pH = min(pH + pEmmit * tax, g)

%Create integrals of specific normal cdfs
bH = @(x) exp(-((x-muH).^2)/(2*sigH.^2)) / (sigH*sqrt(2*pi));
BH = @(y) integral(@(x) bH(x), 0, y);
IntBH = @(z) integral(@(y) BH(y), 0, z, 'ArrayValued',true);
bL = @(x) exp(-((x-muL).^2)/(2*sigL.^2)) / (sigL*sqrt(2*pi));
BL = @(y) integral(@(x) bL(x), 0, y);
IntBL = @(z) integral(@(y) BL(y), 0, z, 'ArrayValued',true);
batSeq = 0.0: 0.5: 6;
genSeq = [35000 40000 50000 60000 68000 70000 75000 80000 90000 100000 110000 135000 150000 175000 200000 225000 250000];

%Case 3
tLBounds = [-(pH*e - pL) (pL + (g-pH)*e)]; %Bounds during which both parties would at least want some battery if it was free
tHBounds = [-(pL*e - pH) (pH + (g-pL)*e)]; %Bounds during which both parties would at least want some battery if it was free
%tLBounds = [65 70];% Quick Run
%tHBounds = [37 42];% Quick Run

bruteForceCounter = 0
for tL = max(fix(tLBounds(1)+1),0) : 10 : fix(tLBounds(2))
    for tH = max(fix(tHBounds(1)+1),0) : 10 : fix(tHBounds(2))
        %Optimal Response Functions for Competitive Setting
        %Fix K
        counter = 0;
        for i = batSeq
            counter = counter + 1;
            fixed_K = i;
            cpAllPartialQFixK = @(Q) cpAllPartialQ(Q, fixed_K, tL, tH)^2; %Squared always to get the zero points
            try
                options = optimoptions('fmincon','Display','off');
                [x,fval,exitflag,output] = fmincon(cpAllPartialQFixK, DH+1000, [], [], [], [] ,0, (DH+DL)/r-DH, [], options);
                cpAllFixKRes(counter,:) = [i, x];
            catch
                disp([i,"None Found"])
            end
        end
        f = figure('visible', 'off');
        plot(cpAllFixKRes(:,1),cpAllFixKRes(:,2));
        hold on %Waits to close grpah until the other response curve is drawn
        disp("Done with FixK")
        %Fix Q
        counter = 0;
        for i = genSeq
            counter = counter + 1;
            fixed_Q = i;
            cpAllPartialKFixQ = @(K) cpAllPartialK(fixed_Q, K, tL, tH)^2;
            try
                options = optimoptions('fmincon','Display','off');
                [x,~,~,~] = fmincon(cpAllPartialKFixQ, 0, [], [], [], [] ,0, 6, [], options);
                cpAllFixQRes(counter,:) = [x, i];
            catch
                disp([i,"None Found"])
            end
        end
        plot(cpAllFixQRes(:,1),cpAllFixQRes(:,2))
        hold off
        legend('Fixed K','Fixed Q')
        fname = sprintf('CompOutCome_%d_%d.jpg', tL,tH)
        saveas(f, fname)%Print pictures
        
        %Store numerical results
        bruteForceCounter = bruteForceCounter+1
        [cpAllIntersectRes(bruteForceCounter,1),cpAllIntersectRes(bruteForceCounter,2)] = cpAllIntersect(cpAllFixKRes, cpAllFixQRes);
        cpAllIntersectRes(bruteForceCounter,3) = tL;
        cpAllIntersectRes(bruteForceCounter,4) = tH;
        toc
    end
end

%Save Intersection points
save('Intersections','cpAllIntersectRes')

%Define all more complicated functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Competition setup


function profit = cpAllPartialQ(Q, K, tL, tH)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = min((Q-DL)/(Q-DH),nDay/0.25);
    
    if Q <= DL %Case 1
        profit = 24 * r *g - cG;
 
    elseif Q <= DH %Case 2

        funDummy = @(k) ((pH*e - pL + tL) * (1-BL(k)) - cS)^2;
        options = optimoptions('fmincon','Display','off');
        [x,~,~,~] = fmincon(funDummy, 0, [], [], [], [] ,0, 6, [], options);

        profit = nNight*(r*pH+r*g) + (pL + (g-pH)*e - tL) * (x - IntBL(x)) - cG;
        
    else %Case 3
        
        profit = max(pH + (g-pL)*e - tH,0) * (K - IntBH(K*V) - K*BH(K*V)*(DL-DH)/(Q-DH)) - ...
        (max(pH + (g-pL)* - tH,0)*sqrt(r)/((DL+DH)/r - DH)) * (K*(2*Q-DH-DL) - 2*(Q-DH)*IntBH(K*V) - K*BH(K*V)*(DL-DH)) + ...
        max(pL + (g-pH)*e - tL,0) * (K-IntBL(K)) * (1 - ((sqrt(r) * (2*Q-DH-DL)) /((DL+DH)/r - DH))) - ...
        cG;
    
    end
end


%Comp - Case 3 - Partial K - Battery
function profit = cpAllPartialK(Q, K, tL, tH)
global r e nDay nNight muH muL sighH sigL DH DL pH pL g cS cG bH BH IntBH bL BL IntBL QLim
    V = min((Q-DL)/(Q-DH),nDay/0.25);
    
    if Q <= DL %Case 1
        
        profit = -1*K; %not really, just force K to be 0.
    
    elseif Q <= DH %Case 2
        
        profit = max(pH*e - pL + tL,0) * (1-BL(K)) - ...
        cS;
        
    else %Case 3
    
        profit = max(pL*e - pH + tH,0) * (1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH))) * (1-BH(K*V))+ ...
        max(pH*e - pL + tL,0) * (1 - ((sqrt(r) * (Q-DH)) /((DL+DH)/r - DH))) * (1-BL(K)) - ...
        cS;
    
    end
end

function [optiK, optiQ] = cpAllIntersect(arr1, arr2)
    global batSeq genSeq DH
    %Set default values
    optiK = 0;
    optiQ = 0;
    
    %Run function to test whether there are any intersecting line segments
    test = lineSegmentIntersect([arr1(1:length(batSeq)-1,:), arr1(2:length(batSeq),:)],...
        [arr2(1:length(genSeq)-1,:), arr2(2:length(genSeq),:)]);
    %If there are, store its values
    if sum(sum(test.("intAdjacencyMatrix"))) == 1
        optiK = test.("intMatrixX")(test.("intAdjacencyMatrix"));
        optiQ = test.("intMatrixY")(test.("intAdjacencyMatrix"));
    elseif sum(sum(test.("intAdjacencyMatrix"))) > 1
        optiK = mean(test.("intMatrixX")(test.("intAdjacencyMatrix")));
        optiQ = mean(test.("intMatrixY")(test.("intAdjacencyMatrix")));
    end
end


function out = lineSegmentIntersect(XY1,XY2)
    % Author:  U. Murat Erdem
    validateattributes(XY1,{'numeric'},{'2d','finite'});
    validateattributes(XY2,{'numeric'},{'2d','finite'});
    [n_rows_1,n_cols_1] = size(XY1);
    [n_rows_2,n_cols_2] = size(XY2);
    if n_cols_1 ~= 4 || n_cols_2 ~= 4
        error('Arguments must be a Nx4 matrices.');
    end
    %%% Prepare matrices for vectorized computation of line intersection points.
    %-------------------------------------------------------------------------------
    X1 = repmat(XY1(:,1),1,n_rows_2);
    X2 = repmat(XY1(:,3),1,n_rows_2);
    Y1 = repmat(XY1(:,2),1,n_rows_2);
    Y2 = repmat(XY1(:,4),1,n_rows_2);
    XY2 = XY2';
    X3 = repmat(XY2(1,:),n_rows_1,1);
    X4 = repmat(XY2(3,:),n_rows_1,1);
    Y3 = repmat(XY2(2,:),n_rows_1,1);
    Y4 = repmat(XY2(4,:),n_rows_1,1);
    X4_X3 = (X4-X3);
    Y1_Y3 = (Y1-Y3);
    Y4_Y3 = (Y4-Y3);
    X1_X3 = (X1-X3);
    X2_X1 = (X2-X1);
    Y2_Y1 = (Y2-Y1);
    numerator_a = X4_X3 .* Y1_Y3 - Y4_Y3 .* X1_X3;
    numerator_b = X2_X1 .* Y1_Y3 - Y2_Y1 .* X1_X3;
    denominator = Y4_Y3 .* X2_X1 - X4_X3 .* Y2_Y1;
    u_a = numerator_a ./ denominator;
    u_b = numerator_b ./ denominator;
    % Find the adjacency matrix A of intersecting lines.
    INT_X = X1+X2_X1.*u_a;
    INT_Y = Y1+Y2_Y1.*u_a;
    INT_B = (u_a >= 0) & (u_a <= 1) & (u_b >= 0) & (u_b <= 1);
    PAR_B = denominator == 0;
    COINC_B = (numerator_a == 0 & numerator_b == 0 & PAR_B);
    % Arrange output.
    out.intAdjacencyMatrix = INT_B;
    out.intMatrixX = INT_X .* INT_B;
    out.intMatrixY = INT_Y .* INT_B;
    out.intNormalizedDistance1To2 = u_a;
    out.intNormalizedDistance2To1 = u_b;
    out.parAdjacencyMatrix = PAR_B;
    out.coincAdjacencyMatrix= COINC_B;
end
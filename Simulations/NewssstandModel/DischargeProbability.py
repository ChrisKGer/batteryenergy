import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

DL = 35000
DH = 65000
r = 0.3
K = 4
nNight = 10
nDay = 24-nNight
eff = .1

def charging_percentage(r, DL, DH, Q, K):
    CNight = np.random.normal(nNight * r, np.sqrt(nNight * r * (1 - r)), 50000)
    CDay = np.random.normal(nDay * r, np.sqrt(nDay * r * (1 - r)), 50000)
    ProdNight = np.random.normal(nNight * r, np.sqrt(nNight * r * (1 - r)), 50000)
    ProdDay = np.random.normal(nDay * r, np.sqrt(nDay * r * (1 - r)), 50000)
    # Get rid of negative values
    CNight = CNight * (CNight > 0)
    CDay = CDay * (CDay > 0)
    ProdNight = ProdNight * (ProdNight > 0)
    ProdDay = ProdDay * (ProdDay > 0)

    # Battery Constraint
    StoreNight = np.minimum(K, CNight) * eff
    StoreDay = np.minimum(K, CDay * (Q - DH) / (Q - DL)) * eff
    StoreNight = StoreNight * (Q - DL) / DH  # How many hours of day discharge were charged at night
    StoreDay = StoreDay * (Q - DH) / DL  # How many hours of night discharge were charged at night

    # What do you actually need
    DCNight = np.minimum(StoreDay, nNight - ProdNight)
    DCDay = np.minimum(StoreNight, nDay - ProdDay)

    if sum([sum(StoreNight), sum(StoreDay)]) == 0:
        DCPerc = 1.0
    else:
        DCPerc = sum([sum(DCNight), sum(DCDay)]) / sum([sum(StoreNight), sum(StoreDay)])

    if sum(StoreNight) == 0:
        DCPercDay = 1.0
    else:
        DCPercDay = sum(DCDay) / sum(StoreNight)

    if sum(StoreDay) == 0:
        DCPercNight = 1.0
    else:
        DCPercNight = sum(DCNight) / sum(StoreDay)

    return DCPerc, DCPercDay, DCPercNight

resArr = np.zeros(shape=(15*50,5))
counter = 0

LL = 66_000
UL = 316_000

for K in range(15):
    print(K)
    for Q in range(LL,UL,5_000):
        resArr[counter,0] = K
        resArr[counter,1] = Q
        resArr[counter,2],resArr[counter,3],resArr[counter,4] = charging_percentage(r, DL, DH, Q, K)
        counter += 1

#Surface Plot
fig = plt.figure()
ax = Axes3D(fig)
X,Y = np.meshgrid(range(15), range(LL,UL,5_000))
ax.plot_surface(X,Y, resArr[:,2].reshape(50,15,order='F'))
ax.set_xlabel('Battery Size in Half-Hours')
plt.yticks(np.arange(LL, UL, 20000))
ax.set_ylabel('Generation in MWh')
#fname='PercentageFull30.png'
#plt.savefig(fname)
plt.show()


%Load all data
cp_ERCOT_0_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_ERCOT_t0.mat')
cp_ERCOT_0_Bat = cp_ERCOT_0_Bat.('Comp_ERCOT_0_Bat')
cp_ERCOT_50_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_ERCOT_t50.mat')
cp_ERCOT_50_Bat = cp_ERCOT_50_Bat.('Comp_ERCOT_50_Bat')
cp_ERCOT_100_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_ERCOT_t100.mat')
cp_ERCOT_100_Bat = cp_ERCOT_100_Bat.('Comp_ERCOT_100_Bat')

cp_GER_0_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_GER_t0.mat')
cp_GER_0_Bat = cp_GER_0_Bat.('Comp_GER_0_Bat')
cp_GER_50_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_GER_t50.mat')
cp_GER_50_Bat = cp_GER_50_Bat.('Comp_GER_50_Bat')
cp_GER_100_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_GER_t100.mat')
cp_GER_100_Bat = cp_GER_100_Bat.('Comp_GER_100_Bat')

cp_PJM_0_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_PJM_t0.mat')
cp_PJM_0_Bat = cp_PJM_0_Bat.('Comp_PJM_0_Bat')
cp_PJM_50_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_PJM_t50.mat')
cp_PJM_50_Bat = cp_PJM_50_Bat.('Comp_PJM_50_Bat')
cp_PJM_100_Bat = load('Results\Comp\AA_Intersections_Results\BatCompResult_PJM_t100.mat')
cp_PJM_100_Bat = cp_PJM_100_Bat.('Comp_PJM_100_Bat')

cp_ERCOT_0_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_ERCOT_t0.mat')
cp_ERCOT_0_Thermal = cp_ERCOT_0_Thermal.('Comp_ERCOT_0_Thermal')
cp_ERCOT_50_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_ERCOT_t50.mat')
cp_ERCOT_50_Thermal = cp_ERCOT_50_Thermal.('Comp_ERCOT_50_Thermal')
cp_ERCOT_100_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_ERCOT_t100.mat')
cp_ERCOT_100_Thermal = cp_ERCOT_100_Thermal.('Comp_ERCOT_100_Thermal')

cp_GER_0_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_GER_t0.mat')
cp_GER_0_Thermal = cp_GER_0_Thermal.('Comp_GER_0_Thermal')
cp_GER_50_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_GER_t50.mat')
cp_GER_50_Thermal = cp_GER_50_Thermal.('Comp_GER_50_Thermal')
cp_GER_100_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_GER_t100.mat')
cp_GER_100_Thermal = cp_GER_100_Thermal.('Comp_GER_100_Thermal')

cp_PJM_0_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_PJM_t0.mat')
cp_PJM_0_Thermal = cp_PJM_0_Thermal.('Comp_PJM_0_Thermal')
cp_PJM_50_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_PJM_t50.mat')
cp_PJM_50_Thermal = cp_PJM_50_Thermal.('Comp_PJM_50_Thermal')
cp_PJM_100_Thermal = load('Results\Comp\AA_Intersections_Results\ThermalCompResult_PJM_t100.mat')
cp_PJM_100_Thermal = cp_PJM_100_Thermal.('Comp_PJM_100_Thermal')

cpResList = {cp_ERCOT_0_Bat cp_ERCOT_50_Bat cp_ERCOT_100_Bat  ...
    cp_GER_0_Bat cp_GER_50_Bat cp_GER_100_Bat  ...
    cp_PJM_0_Bat cp_PJM_50_Bat cp_PJM_100_Bat ...
    cp_ERCOT_0_Thermal cp_ERCOT_50_Thermal cp_ERCOT_100_Thermal ...
    cp_GER_0_Thermal cp_GER_50_Thermal cp_GER_100_Thermal ...
    cp_PJM_0_Thermal cp_PJM_50_Thermal cp_PJM_100_Thermal};

for i = 1:18
    cp3IntersectRes = cpResList{i};
    redEqu = cp3IntersectRes(cp3IntersectRes(:,1)<=0.25,:);
    greenEqu = cp3IntersectRes(cp3IntersectRes(:,1)>0.25,:);
    f = figure('visible', 'off');
    hold on;
    scatter(redEqu(:,3),redEqu(:,4),7,'r',"filled");
    scatter(greenEqu(:,3),greenEqu(:,4),7,'g',"filled");
    hold off;
    legend({'Border-Case Equilibrium','Proper Equilibrium'},'Location','southwest');
    xlabel('Low transfer price tL') ;
    ylabel('High transfer price tH') ;
    fname = sprintf('CompEquilibrium2D%d.jpg',i)
    saveas(f, fname);%Print pictures;
end

for i = 1:18
    cp3IntersectRes = cpResList{i};
    f = figure('visible', 'off');
    hold on;
    scatter(cp3IntersectRes(:,1),cp3IntersectRes(:,2));
    hold off;
    xlabel('Storage capacity in hours') ;
    ylabel('Generation capacity in MWh') ;
    fname = sprintf('CompEquilibriumAll%d.jpg',i)
    saveas(f, fname);%Print pictures;
end

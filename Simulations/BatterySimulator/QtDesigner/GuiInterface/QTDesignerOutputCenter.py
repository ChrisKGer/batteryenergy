# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'centerscreen.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CenterScreen(object):
    def setupUi(self, CenterScreen):
        CenterScreen.setObjectName("CenterScreen")
        CenterScreen.resize(1152, 669)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(CenterScreen.sizePolicy().hasHeightForWidth())
        CenterScreen.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(CenterScreen)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.btnOption1 = QtWidgets.QPushButton(self.centralwidget)
        self.btnOption1.setObjectName("btnOption1")
        self.gridLayout.addWidget(self.btnOption1, 0, 1, 1, 1)
        self.btnOption2 = QtWidgets.QPushButton(self.centralwidget)
        self.btnOption2.setObjectName("btnOption2")
        self.gridLayout.addWidget(self.btnOption2, 1, 1, 1, 1)
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.stackedWidget.sizePolicy().hasHeightForWidth())
        self.stackedWidget.setSizePolicy(sizePolicy)
        self.stackedWidget.setObjectName("stackedWidget")
        self.page = QtWidgets.QWidget()
        self.page.setObjectName("page")
        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.stackedWidget.addWidget(self.page_2)
        self.gridLayout.addWidget(self.stackedWidget, 1, 0, 1, 1)
        CenterScreen.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(CenterScreen)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1152, 21))
        self.menubar.setObjectName("menubar")
        CenterScreen.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(CenterScreen)
        self.statusbar.setObjectName("statusbar")
        CenterScreen.setStatusBar(self.statusbar)
        self.menu_SamplePath = QtWidgets.QAction(CenterScreen)
        self.menu_SamplePath.setObjectName("menu_SamplePath")
        self.menu_SwitchedPoisson = QtWidgets.QAction(CenterScreen)
        self.menu_SwitchedPoisson.setObjectName("menu_SwitchedPoisson")

        self.retranslateUi(CenterScreen)
        QtCore.QMetaObject.connectSlotsByName(CenterScreen)

    def retranslateUi(self, CenterScreen):
        _translate = QtCore.QCoreApplication.translate
        CenterScreen.setWindowTitle(_translate("CenterScreen", "MainWindow"))
        self.btnOption1.setText(_translate("CenterScreen", "Sample Path"))
        self.btnOption2.setText(_translate("CenterScreen", "Switched Poisson Process"))
        self.menu_SamplePath.setText(_translate("CenterScreen", "Sample Path"))
        self.menu_SwitchedPoisson.setText(_translate("CenterScreen", "Switched Poisson"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CenterScreen = QtWidgets.QMainWindow()
    ui = Ui_CenterScreen()
    ui.setupUi(CenterScreen)
    CenterScreen.show()
    sys.exit(app.exec_())


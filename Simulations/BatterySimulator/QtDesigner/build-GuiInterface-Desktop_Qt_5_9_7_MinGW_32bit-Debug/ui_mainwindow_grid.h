/********************************************************************************
** Form generated from reading UI file 'mainwindow_grid.ui'
**
** Created by: Qt User Interface Compiler version 5.9.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_GRID_H
#define UI_MAINWINDOW_GRID_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *menu_SamplePath;
    QAction *menu_SwitchedPoisson;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QWidget *SamplePathPlot;
    QLabel *lblWhartonLogo;
    QHBoxLayout *horizontalLayout_3;
    QRadioButton *rbArr3;
    QGridLayout *gridLayout_3;
    QLabel *labArr31;
    QLabel *labArr32;
    QLineEdit *fieldArr31;
    QLineEdit *fieldArr32;
    QHBoxLayout *horizontalLayout_6;
    QRadioButton *rbProc2;
    QGridLayout *gridLayout_5;
    QLabel *labProc21;
    QLabel *labProc22;
    QLineEdit *fieldProc21;
    QLineEdit *fieldProc22;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_7;
    QRadioButton *rbProc3;
    QGridLayout *gridLayout_6;
    QLineEdit *fieldProc32;
    QLineEdit *fieldProc31;
    QLabel *labProc31;
    QLabel *labProc32;
    QLabel *lblVersion;
    QHBoxLayout *horizontalLayout_8;
    QRadioButton *rbProc4;
    QGridLayout *gridLayout_7;
    QLabel *labProc42;
    QLabel *labProc41;
    QLineEdit *fieldProc41;
    QLineEdit *fieldProc42;
    QPushButton *btnSimulate;
    QLabel *lblHeader;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *rbArr2;
    QGridLayout *gridLayout_2;
    QLabel *labArr21;
    QLabel *labArr22;
    QLineEdit *fieldArr21;
    QLineEdit *fieldArr22;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_4;
    QRadioButton *rbArr4;
    QGridLayout *gridLayout_4;
    QLabel *labArr42;
    QLabel *labArr41;
    QLineEdit *fieldArr41;
    QLineEdit *fieldArr42;
    QHBoxLayout *horizontalLayout_5;
    QRadioButton *rbProc1;
    QHBoxLayout *horizontalLayout_10;
    QLabel *labProc1;
    QLineEdit *fieldProc1;
    QLabel *lblModuleName;
    QHBoxLayout *horizontalLayout;
    QRadioButton *rbArr1;
    QHBoxLayout *horizontalLayout_9;
    QLabel *labArr1;
    QLineEdit *fieldArr1;
    QWidget *StatePlot;
    QHBoxLayout *horizontalLayout_11;
    QSlider *sliderPeriods;
    QLabel *lblNumPeriods;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1152, 648);
        menu_SamplePath = new QAction(MainWindow);
        menu_SamplePath->setObjectName(QStringLiteral("menu_SamplePath"));
        menu_SwitchedPoisson = new QAction(MainWindow);
        menu_SwitchedPoisson->setObjectName(QStringLiteral("menu_SwitchedPoisson"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        SamplePathPlot = new QWidget(centralWidget);
        SamplePathPlot->setObjectName(QStringLiteral("SamplePathPlot"));

        gridLayout->addWidget(SamplePathPlot, 3, 1, 9, 1);

        lblWhartonLogo = new QLabel(centralWidget);
        lblWhartonLogo->setObjectName(QStringLiteral("lblWhartonLogo"));

        gridLayout->addWidget(lblWhartonLogo, 1, 2, 2, 1, Qt::AlignHCenter);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, 3, -1, 3);
        rbArr3 = new QRadioButton(centralWidget);
        rbArr3->setObjectName(QStringLiteral("rbArr3"));

        horizontalLayout_3->addWidget(rbArr3);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        labArr31 = new QLabel(centralWidget);
        labArr31->setObjectName(QStringLiteral("labArr31"));

        gridLayout_3->addWidget(labArr31, 0, 0, 1, 1, Qt::AlignHCenter);

        labArr32 = new QLabel(centralWidget);
        labArr32->setObjectName(QStringLiteral("labArr32"));

        gridLayout_3->addWidget(labArr32, 1, 0, 1, 1, Qt::AlignHCenter);

        fieldArr31 = new QLineEdit(centralWidget);
        fieldArr31->setObjectName(QStringLiteral("fieldArr31"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fieldArr31->sizePolicy().hasHeightForWidth());
        fieldArr31->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(fieldArr31, 0, 1, 1, 1);

        fieldArr32 = new QLineEdit(centralWidget);
        fieldArr32->setObjectName(QStringLiteral("fieldArr32"));
        sizePolicy.setHeightForWidth(fieldArr32->sizePolicy().hasHeightForWidth());
        fieldArr32->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(fieldArr32, 1, 1, 1, 1);

        gridLayout_3->setColumnStretch(0, 1);
        gridLayout_3->setColumnStretch(1, 1);

        horizontalLayout_3->addLayout(gridLayout_3);

        horizontalLayout_3->setStretch(0, 3);
        horizontalLayout_3->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_3, 5, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, 3, -1, 3);
        rbProc2 = new QRadioButton(centralWidget);
        rbProc2->setObjectName(QStringLiteral("rbProc2"));

        horizontalLayout_6->addWidget(rbProc2);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        labProc21 = new QLabel(centralWidget);
        labProc21->setObjectName(QStringLiteral("labProc21"));

        gridLayout_5->addWidget(labProc21, 0, 0, 1, 1, Qt::AlignHCenter);

        labProc22 = new QLabel(centralWidget);
        labProc22->setObjectName(QStringLiteral("labProc22"));

        gridLayout_5->addWidget(labProc22, 1, 0, 1, 1, Qt::AlignHCenter);

        fieldProc21 = new QLineEdit(centralWidget);
        fieldProc21->setObjectName(QStringLiteral("fieldProc21"));
        sizePolicy.setHeightForWidth(fieldProc21->sizePolicy().hasHeightForWidth());
        fieldProc21->setSizePolicy(sizePolicy);

        gridLayout_5->addWidget(fieldProc21, 0, 1, 1, 1);

        fieldProc22 = new QLineEdit(centralWidget);
        fieldProc22->setObjectName(QStringLiteral("fieldProc22"));
        sizePolicy.setHeightForWidth(fieldProc22->sizePolicy().hasHeightForWidth());
        fieldProc22->setSizePolicy(sizePolicy);

        gridLayout_5->addWidget(fieldProc22, 1, 1, 1, 1);

        gridLayout_5->setColumnStretch(0, 1);
        gridLayout_5->setColumnStretch(1, 1);

        horizontalLayout_6->addLayout(gridLayout_5);

        horizontalLayout_6->setStretch(0, 3);
        horizontalLayout_6->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_6, 9, 0, 1, 1);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);

        gridLayout->addWidget(label_4, 2, 0, 1, 1, Qt::AlignHCenter);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(-1, 3, -1, 3);
        rbProc3 = new QRadioButton(centralWidget);
        rbProc3->setObjectName(QStringLiteral("rbProc3"));

        horizontalLayout_7->addWidget(rbProc3);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        fieldProc32 = new QLineEdit(centralWidget);
        fieldProc32->setObjectName(QStringLiteral("fieldProc32"));
        sizePolicy.setHeightForWidth(fieldProc32->sizePolicy().hasHeightForWidth());
        fieldProc32->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(fieldProc32, 1, 1, 1, 1);

        fieldProc31 = new QLineEdit(centralWidget);
        fieldProc31->setObjectName(QStringLiteral("fieldProc31"));
        sizePolicy.setHeightForWidth(fieldProc31->sizePolicy().hasHeightForWidth());
        fieldProc31->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(fieldProc31, 0, 1, 1, 1);

        labProc31 = new QLabel(centralWidget);
        labProc31->setObjectName(QStringLiteral("labProc31"));

        gridLayout_6->addWidget(labProc31, 0, 0, 1, 1, Qt::AlignHCenter);

        labProc32 = new QLabel(centralWidget);
        labProc32->setObjectName(QStringLiteral("labProc32"));

        gridLayout_6->addWidget(labProc32, 1, 0, 1, 1, Qt::AlignHCenter);

        gridLayout_6->setColumnStretch(0, 1);
        gridLayout_6->setColumnStretch(1, 1);

        horizontalLayout_7->addLayout(gridLayout_6);

        horizontalLayout_7->setStretch(0, 3);
        horizontalLayout_7->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_7, 10, 0, 1, 1);

        lblVersion = new QLabel(centralWidget);
        lblVersion->setObjectName(QStringLiteral("lblVersion"));

        gridLayout->addWidget(lblVersion, 0, 2, 1, 1, Qt::AlignRight|Qt::AlignTop);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(-1, 3, -1, 3);
        rbProc4 = new QRadioButton(centralWidget);
        rbProc4->setObjectName(QStringLiteral("rbProc4"));

        horizontalLayout_8->addWidget(rbProc4);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        labProc42 = new QLabel(centralWidget);
        labProc42->setObjectName(QStringLiteral("labProc42"));

        gridLayout_7->addWidget(labProc42, 1, 0, 1, 1, Qt::AlignHCenter);

        labProc41 = new QLabel(centralWidget);
        labProc41->setObjectName(QStringLiteral("labProc41"));

        gridLayout_7->addWidget(labProc41, 0, 0, 1, 1, Qt::AlignHCenter);

        fieldProc41 = new QLineEdit(centralWidget);
        fieldProc41->setObjectName(QStringLiteral("fieldProc41"));
        sizePolicy.setHeightForWidth(fieldProc41->sizePolicy().hasHeightForWidth());
        fieldProc41->setSizePolicy(sizePolicy);

        gridLayout_7->addWidget(fieldProc41, 0, 1, 1, 1);

        fieldProc42 = new QLineEdit(centralWidget);
        fieldProc42->setObjectName(QStringLiteral("fieldProc42"));
        sizePolicy.setHeightForWidth(fieldProc42->sizePolicy().hasHeightForWidth());
        fieldProc42->setSizePolicy(sizePolicy);

        gridLayout_7->addWidget(fieldProc42, 1, 1, 1, 1);

        gridLayout_7->setColumnStretch(0, 1);
        gridLayout_7->setColumnStretch(1, 1);

        horizontalLayout_8->addLayout(gridLayout_7);

        horizontalLayout_8->setStretch(0, 3);
        horizontalLayout_8->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_8, 11, 0, 1, 1);

        btnSimulate = new QPushButton(centralWidget);
        btnSimulate->setObjectName(QStringLiteral("btnSimulate"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnSimulate->sizePolicy().hasHeightForWidth());
        btnSimulate->setSizePolicy(sizePolicy1);
        btnSimulate->setToolTipDuration(1);

        gridLayout->addWidget(btnSimulate, 2, 1, 1, 1);

        lblHeader = new QLabel(centralWidget);
        lblHeader->setObjectName(QStringLiteral("lblHeader"));
        QFont font1;
        font1.setPointSize(12);
        lblHeader->setFont(font1);

        gridLayout->addWidget(lblHeader, 0, 1, 1, 1, Qt::AlignHCenter);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 3, -1, 3);
        rbArr2 = new QRadioButton(centralWidget);
        rbArr2->setObjectName(QStringLiteral("rbArr2"));

        horizontalLayout_2->addWidget(rbArr2);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        labArr21 = new QLabel(centralWidget);
        labArr21->setObjectName(QStringLiteral("labArr21"));

        gridLayout_2->addWidget(labArr21, 0, 0, 1, 1, Qt::AlignHCenter);

        labArr22 = new QLabel(centralWidget);
        labArr22->setObjectName(QStringLiteral("labArr22"));

        gridLayout_2->addWidget(labArr22, 1, 0, 1, 1, Qt::AlignHCenter);

        fieldArr21 = new QLineEdit(centralWidget);
        fieldArr21->setObjectName(QStringLiteral("fieldArr21"));
        sizePolicy.setHeightForWidth(fieldArr21->sizePolicy().hasHeightForWidth());
        fieldArr21->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(fieldArr21, 0, 1, 1, 1);

        fieldArr22 = new QLineEdit(centralWidget);
        fieldArr22->setObjectName(QStringLiteral("fieldArr22"));
        sizePolicy.setHeightForWidth(fieldArr22->sizePolicy().hasHeightForWidth());
        fieldArr22->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(fieldArr22, 1, 1, 1, 1);

        gridLayout_2->setColumnStretch(0, 1);
        gridLayout_2->setColumnStretch(1, 1);

        horizontalLayout_2->addLayout(gridLayout_2);

        horizontalLayout_2->setStretch(0, 3);
        horizontalLayout_2->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_2, 4, 0, 1, 1);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font);

        gridLayout->addWidget(label_5, 7, 0, 1, 1, Qt::AlignHCenter);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, 3, -1, 3);
        rbArr4 = new QRadioButton(centralWidget);
        rbArr4->setObjectName(QStringLiteral("rbArr4"));

        horizontalLayout_4->addWidget(rbArr4);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        labArr42 = new QLabel(centralWidget);
        labArr42->setObjectName(QStringLiteral("labArr42"));

        gridLayout_4->addWidget(labArr42, 1, 0, 1, 1, Qt::AlignHCenter);

        labArr41 = new QLabel(centralWidget);
        labArr41->setObjectName(QStringLiteral("labArr41"));

        gridLayout_4->addWidget(labArr41, 0, 0, 1, 1, Qt::AlignHCenter);

        fieldArr41 = new QLineEdit(centralWidget);
        fieldArr41->setObjectName(QStringLiteral("fieldArr41"));
        sizePolicy.setHeightForWidth(fieldArr41->sizePolicy().hasHeightForWidth());
        fieldArr41->setSizePolicy(sizePolicy);

        gridLayout_4->addWidget(fieldArr41, 0, 1, 1, 1);

        fieldArr42 = new QLineEdit(centralWidget);
        fieldArr42->setObjectName(QStringLiteral("fieldArr42"));
        sizePolicy.setHeightForWidth(fieldArr42->sizePolicy().hasHeightForWidth());
        fieldArr42->setSizePolicy(sizePolicy);

        gridLayout_4->addWidget(fieldArr42, 1, 1, 1, 1);

        gridLayout_4->setColumnStretch(0, 1);
        gridLayout_4->setColumnStretch(1, 1);

        horizontalLayout_4->addLayout(gridLayout_4);

        horizontalLayout_4->setStretch(0, 3);
        horizontalLayout_4->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_4, 6, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(-1, 3, -1, 3);
        rbProc1 = new QRadioButton(centralWidget);
        rbProc1->setObjectName(QStringLiteral("rbProc1"));
        rbProc1->setChecked(true);

        horizontalLayout_5->addWidget(rbProc1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        labProc1 = new QLabel(centralWidget);
        labProc1->setObjectName(QStringLiteral("labProc1"));

        horizontalLayout_10->addWidget(labProc1, 0, Qt::AlignHCenter);

        fieldProc1 = new QLineEdit(centralWidget);
        fieldProc1->setObjectName(QStringLiteral("fieldProc1"));
        sizePolicy.setHeightForWidth(fieldProc1->sizePolicy().hasHeightForWidth());
        fieldProc1->setSizePolicy(sizePolicy);

        horizontalLayout_10->addWidget(fieldProc1);

        horizontalLayout_10->setStretch(0, 1);
        horizontalLayout_10->setStretch(1, 1);

        horizontalLayout_5->addLayout(horizontalLayout_10);

        horizontalLayout_5->setStretch(0, 3);
        horizontalLayout_5->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_5, 8, 0, 1, 1);

        lblModuleName = new QLabel(centralWidget);
        lblModuleName->setObjectName(QStringLiteral("lblModuleName"));
        QFont font2;
        font2.setPointSize(10);
        lblModuleName->setFont(font2);

        gridLayout->addWidget(lblModuleName, 0, 0, 1, 1, Qt::AlignHCenter);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 3, -1, 3);
        rbArr1 = new QRadioButton(centralWidget);
        rbArr1->setObjectName(QStringLiteral("rbArr1"));
        rbArr1->setChecked(true);

        horizontalLayout->addWidget(rbArr1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        labArr1 = new QLabel(centralWidget);
        labArr1->setObjectName(QStringLiteral("labArr1"));

        horizontalLayout_9->addWidget(labArr1, 0, Qt::AlignHCenter);

        fieldArr1 = new QLineEdit(centralWidget);
        fieldArr1->setObjectName(QStringLiteral("fieldArr1"));
        sizePolicy.setHeightForWidth(fieldArr1->sizePolicy().hasHeightForWidth());
        fieldArr1->setSizePolicy(sizePolicy);

        horizontalLayout_9->addWidget(fieldArr1);

        horizontalLayout_9->setStretch(0, 1);
        horizontalLayout_9->setStretch(1, 1);

        horizontalLayout->addLayout(horizontalLayout_9);

        horizontalLayout->setStretch(0, 3);
        horizontalLayout->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout, 3, 0, 1, 1);

        StatePlot = new QWidget(centralWidget);
        StatePlot->setObjectName(QStringLiteral("StatePlot"));

        gridLayout->addWidget(StatePlot, 3, 2, 9, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        sliderPeriods = new QSlider(centralWidget);
        sliderPeriods->setObjectName(QStringLiteral("sliderPeriods"));
        sliderPeriods->setMinimum(1);
        sliderPeriods->setMaximum(1000);
        sliderPeriods->setValue(100);
        sliderPeriods->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(sliderPeriods);

        lblNumPeriods = new QLabel(centralWidget);
        lblNumPeriods->setObjectName(QStringLiteral("lblNumPeriods"));

        horizontalLayout_11->addWidget(lblNumPeriods, 0, Qt::AlignHCenter);

        horizontalLayout_11->setStretch(0, 5);
        horizontalLayout_11->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout_11, 1, 1, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(1, 1);
        gridLayout->setRowStretch(2, 1);
        gridLayout->setRowStretch(3, 1);
        gridLayout->setRowStretch(4, 1);
        gridLayout->setRowStretch(5, 1);
        gridLayout->setRowStretch(6, 1);
        gridLayout->setRowStretch(7, 1);
        gridLayout->setRowStretch(8, 1);
        gridLayout->setRowStretch(9, 1);
        gridLayout->setRowStretch(10, 1);
        gridLayout->setRowStretch(11, 1);
        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 3);
        gridLayout->setColumnStretch(2, 1);
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PhD or Bust", Q_NULLPTR));
        menu_SamplePath->setText(QApplication::translate("MainWindow", "Sample Path", Q_NULLPTR));
        menu_SwitchedPoisson->setText(QApplication::translate("MainWindow", "Switched Poisson", Q_NULLPTR));
        lblWhartonLogo->setText(QApplication::translate("MainWindow", "Picture Placeholder", Q_NULLPTR));
        rbArr3->setText(QApplication::translate("MainWindow", "Uniform", Q_NULLPTR));
        labArr31->setText(QApplication::translate("MainWindow", "Lower Lim", Q_NULLPTR));
        labArr32->setText(QApplication::translate("MainWindow", "Upper Lim", Q_NULLPTR));
        rbProc2->setText(QApplication::translate("MainWindow", "Normal", Q_NULLPTR));
        labProc21->setText(QApplication::translate("MainWindow", "Mean", Q_NULLPTR));
        labProc22->setText(QApplication::translate("MainWindow", "Std. Dev.", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Arrival Rate", Q_NULLPTR));
        rbProc3->setText(QApplication::translate("MainWindow", "Uniform", Q_NULLPTR));
        labProc31->setText(QApplication::translate("MainWindow", "Lower Lim", Q_NULLPTR));
        labProc32->setText(QApplication::translate("MainWindow", "Upper Lim", Q_NULLPTR));
        lblVersion->setText(QApplication::translate("MainWindow", "CK Sim v0.2", Q_NULLPTR));
        rbProc4->setText(QApplication::translate("MainWindow", "Erlang", Q_NULLPTR));
        labProc42->setText(QApplication::translate("MainWindow", "Rate", Q_NULLPTR));
        labProc41->setText(QApplication::translate("MainWindow", "k", Q_NULLPTR));
        btnSimulate->setText(QApplication::translate("MainWindow", "Simulate", Q_NULLPTR));
        lblHeader->setText(QApplication::translate("MainWindow", "Battery Simulator", Q_NULLPTR));
        rbArr2->setText(QApplication::translate("MainWindow", "Normal", Q_NULLPTR));
        labArr21->setText(QApplication::translate("MainWindow", "Mean", Q_NULLPTR));
        labArr22->setText(QApplication::translate("MainWindow", "Std. Dev.", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Processing Rate", Q_NULLPTR));
        rbArr4->setText(QApplication::translate("MainWindow", "Erlang", Q_NULLPTR));
        labArr42->setText(QApplication::translate("MainWindow", "Rate", Q_NULLPTR));
        labArr41->setText(QApplication::translate("MainWindow", "k", Q_NULLPTR));
        rbProc1->setText(QApplication::translate("MainWindow", "Exponential", Q_NULLPTR));
        labProc1->setText(QApplication::translate("MainWindow", "Rate", Q_NULLPTR));
        lblModuleName->setText(QApplication::translate("MainWindow", "Sample Path Module", Q_NULLPTR));
        rbArr1->setText(QApplication::translate("MainWindow", "Exponential", Q_NULLPTR));
        labArr1->setText(QApplication::translate("MainWindow", "Rate", Q_NULLPTR));
        lblNumPeriods->setText(QApplication::translate("MainWindow", "numPeriods", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_GRID_H

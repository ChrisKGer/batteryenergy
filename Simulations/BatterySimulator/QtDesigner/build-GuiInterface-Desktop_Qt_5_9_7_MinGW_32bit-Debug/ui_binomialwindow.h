/********************************************************************************
** Form generated from reading UI file 'binomialwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BINOMIALWINDOW_H
#define UI_BINOMIALWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *menu_SamplePath;
    QAction *menu_SwitchedPoisson;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_8;
    QRadioButton *rbProc3;
    QHBoxLayout *horizontalLayout_10;
    QLabel *labProc3;
    QLineEdit *fieldProc3;
    QLabel *lblVersion;
    QWidget *StatePlot;
    QWidget *SamplePathPlot;
    QHBoxLayout *horizontalLayout_6;
    QRadioButton *rbProc1;
    QGridLayout *gridLayout_5;
    QLabel *labProc11;
    QLabel *labProc12;
    QLineEdit *fieldProc11;
    QLineEdit *fieldProc12;
    QLabel *label_4;
    QFrame *line_2;
    QFrame *line;
    QLabel *lblHeader;
    QLabel *lblWhartonLogo;
    QHBoxLayout *horizontalLayout;
    QLabel *labArr1;
    QLineEdit *fieldArr1;
    QHBoxLayout *horizontalLayout_11;
    QSlider *sliderPeriods;
    QLabel *lblNumPeriods;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labArr3;
    QLineEdit *fieldArr3;
    QHBoxLayout *horizontalLayout_7;
    QRadioButton *rbProc2;
    QGridLayout *gridLayout_6;
    QLineEdit *fieldProc22;
    QLineEdit *fieldProc21;
    QLabel *labProc21;
    QLabel *labProc22;
    QHBoxLayout *horizontalLayout_4;
    QRadioButton *radioBtn10;
    QRadioButton *radioBtn12;
    QLabel *label_6;
    QLabel *label_5;
    QPushButton *btnSimulate;
    QHBoxLayout *horizontalLayout_5;
    QSlider *sliderBatSize;
    QLabel *lblBatSize;
    QLabel *lblModuleName;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labArr2;
    QLineEdit *fieldArr2;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1152, 648);
        menu_SamplePath = new QAction(MainWindow);
        menu_SamplePath->setObjectName(QStringLiteral("menu_SamplePath"));
        menu_SwitchedPoisson = new QAction(MainWindow);
        menu_SwitchedPoisson->setObjectName(QStringLiteral("menu_SwitchedPoisson"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(-1, 3, -1, 3);
        rbProc3 = new QRadioButton(centralWidget);
        rbProc3->setObjectName(QStringLiteral("rbProc3"));

        horizontalLayout_8->addWidget(rbProc3);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        labProc3 = new QLabel(centralWidget);
        labProc3->setObjectName(QStringLiteral("labProc3"));

        horizontalLayout_10->addWidget(labProc3, 0, Qt::AlignHCenter);

        fieldProc3 = new QLineEdit(centralWidget);
        fieldProc3->setObjectName(QStringLiteral("fieldProc3"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fieldProc3->sizePolicy().hasHeightForWidth());
        fieldProc3->setSizePolicy(sizePolicy);

        horizontalLayout_10->addWidget(fieldProc3);

        horizontalLayout_10->setStretch(0, 1);
        horizontalLayout_10->setStretch(1, 1);

        horizontalLayout_8->addLayout(horizontalLayout_10);

        horizontalLayout_8->setStretch(0, 4);
        horizontalLayout_8->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_8, 14, 0, 1, 1);

        lblVersion = new QLabel(centralWidget);
        lblVersion->setObjectName(QStringLiteral("lblVersion"));

        gridLayout->addWidget(lblVersion, 0, 2, 1, 1, Qt::AlignRight|Qt::AlignTop);

        StatePlot = new QWidget(centralWidget);
        StatePlot->setObjectName(QStringLiteral("StatePlot"));

        gridLayout->addWidget(StatePlot, 3, 2, 12, 1);

        SamplePathPlot = new QWidget(centralWidget);
        SamplePathPlot->setObjectName(QStringLiteral("SamplePathPlot"));

        gridLayout->addWidget(SamplePathPlot, 3, 1, 12, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, 3, -1, 3);
        rbProc1 = new QRadioButton(centralWidget);
        rbProc1->setObjectName(QStringLiteral("rbProc1"));

        horizontalLayout_6->addWidget(rbProc1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        labProc11 = new QLabel(centralWidget);
        labProc11->setObjectName(QStringLiteral("labProc11"));

        gridLayout_5->addWidget(labProc11, 0, 0, 1, 1, Qt::AlignHCenter);

        labProc12 = new QLabel(centralWidget);
        labProc12->setObjectName(QStringLiteral("labProc12"));

        gridLayout_5->addWidget(labProc12, 1, 0, 1, 1, Qt::AlignHCenter);

        fieldProc11 = new QLineEdit(centralWidget);
        fieldProc11->setObjectName(QStringLiteral("fieldProc11"));
        sizePolicy.setHeightForWidth(fieldProc11->sizePolicy().hasHeightForWidth());
        fieldProc11->setSizePolicy(sizePolicy);

        gridLayout_5->addWidget(fieldProc11, 0, 1, 1, 1);

        fieldProc12 = new QLineEdit(centralWidget);
        fieldProc12->setObjectName(QStringLiteral("fieldProc12"));
        sizePolicy.setHeightForWidth(fieldProc12->sizePolicy().hasHeightForWidth());
        fieldProc12->setSizePolicy(sizePolicy);

        gridLayout_5->addWidget(fieldProc12, 1, 1, 1, 1);

        gridLayout_5->setColumnStretch(0, 1);
        gridLayout_5->setColumnStretch(1, 1);

        horizontalLayout_6->addLayout(gridLayout_5);

        horizontalLayout_6->setStretch(0, 4);
        horizontalLayout_6->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_6, 12, 0, 1, 1);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);

        gridLayout->addWidget(label_4, 2, 0, 1, 1, Qt::AlignHCenter);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 10, 0, 1, 1);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 7, 0, 1, 1);

        lblHeader = new QLabel(centralWidget);
        lblHeader->setObjectName(QStringLiteral("lblHeader"));
        QFont font1;
        font1.setPointSize(12);
        lblHeader->setFont(font1);

        gridLayout->addWidget(lblHeader, 0, 1, 1, 1, Qt::AlignHCenter);

        lblWhartonLogo = new QLabel(centralWidget);
        lblWhartonLogo->setObjectName(QStringLiteral("lblWhartonLogo"));

        gridLayout->addWidget(lblWhartonLogo, 1, 2, 2, 1, Qt::AlignHCenter);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 3, -1, 3);
        labArr1 = new QLabel(centralWidget);
        labArr1->setObjectName(QStringLiteral("labArr1"));

        horizontalLayout->addWidget(labArr1, 0, Qt::AlignHCenter);

        fieldArr1 = new QLineEdit(centralWidget);
        fieldArr1->setObjectName(QStringLiteral("fieldArr1"));
        sizePolicy.setHeightForWidth(fieldArr1->sizePolicy().hasHeightForWidth());
        fieldArr1->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(fieldArr1);

        horizontalLayout->setStretch(0, 2);
        horizontalLayout->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout, 3, 0, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        sliderPeriods = new QSlider(centralWidget);
        sliderPeriods->setObjectName(QStringLiteral("sliderPeriods"));
        sliderPeriods->setMinimum(1);
        sliderPeriods->setMaximum(1000);
        sliderPeriods->setValue(100);
        sliderPeriods->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(sliderPeriods);

        lblNumPeriods = new QLabel(centralWidget);
        lblNumPeriods->setObjectName(QStringLiteral("lblNumPeriods"));

        horizontalLayout_11->addWidget(lblNumPeriods, 0, Qt::AlignHCenter);

        horizontalLayout_11->setStretch(0, 5);
        horizontalLayout_11->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout_11, 1, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, 3, -1, 3);
        labArr3 = new QLabel(centralWidget);
        labArr3->setObjectName(QStringLiteral("labArr3"));

        horizontalLayout_3->addWidget(labArr3, 0, Qt::AlignHCenter);

        fieldArr3 = new QLineEdit(centralWidget);
        fieldArr3->setObjectName(QStringLiteral("fieldArr3"));
        sizePolicy.setHeightForWidth(fieldArr3->sizePolicy().hasHeightForWidth());
        fieldArr3->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(fieldArr3);

        horizontalLayout_3->setStretch(0, 2);
        horizontalLayout_3->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout_3, 5, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(-1, 3, -1, 3);
        rbProc2 = new QRadioButton(centralWidget);
        rbProc2->setObjectName(QStringLiteral("rbProc2"));

        horizontalLayout_7->addWidget(rbProc2);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        fieldProc22 = new QLineEdit(centralWidget);
        fieldProc22->setObjectName(QStringLiteral("fieldProc22"));
        sizePolicy.setHeightForWidth(fieldProc22->sizePolicy().hasHeightForWidth());
        fieldProc22->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(fieldProc22, 1, 1, 1, 1);

        fieldProc21 = new QLineEdit(centralWidget);
        fieldProc21->setObjectName(QStringLiteral("fieldProc21"));
        sizePolicy.setHeightForWidth(fieldProc21->sizePolicy().hasHeightForWidth());
        fieldProc21->setSizePolicy(sizePolicy);

        gridLayout_6->addWidget(fieldProc21, 0, 1, 1, 1);

        labProc21 = new QLabel(centralWidget);
        labProc21->setObjectName(QStringLiteral("labProc21"));

        gridLayout_6->addWidget(labProc21, 0, 0, 1, 1, Qt::AlignHCenter);

        labProc22 = new QLabel(centralWidget);
        labProc22->setObjectName(QStringLiteral("labProc22"));

        gridLayout_6->addWidget(labProc22, 1, 0, 1, 1, Qt::AlignHCenter);

        gridLayout_6->setColumnStretch(0, 1);
        gridLayout_6->setColumnStretch(1, 1);

        horizontalLayout_7->addLayout(gridLayout_6);

        horizontalLayout_7->setStretch(0, 4);
        horizontalLayout_7->setStretch(1, 5);

        gridLayout->addLayout(horizontalLayout_7, 13, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, 3, -1, 3);
        radioBtn10 = new QRadioButton(centralWidget);
        radioBtn10->setObjectName(QStringLiteral("radioBtn10"));

        horizontalLayout_4->addWidget(radioBtn10, 0, Qt::AlignHCenter);

        radioBtn12 = new QRadioButton(centralWidget);
        radioBtn12->setObjectName(QStringLiteral("radioBtn12"));

        horizontalLayout_4->addWidget(radioBtn12, 0, Qt::AlignHCenter);


        gridLayout->addLayout(horizontalLayout_4, 6, 0, 1, 1);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font);

        gridLayout->addWidget(label_6, 8, 0, 1, 1, Qt::AlignHCenter);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font);

        gridLayout->addWidget(label_5, 11, 0, 1, 1, Qt::AlignHCenter);

        btnSimulate = new QPushButton(centralWidget);
        btnSimulate->setObjectName(QStringLiteral("btnSimulate"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnSimulate->sizePolicy().hasHeightForWidth());
        btnSimulate->setSizePolicy(sizePolicy1);
        btnSimulate->setToolTipDuration(1);

        gridLayout->addWidget(btnSimulate, 2, 1, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(10, 3, -1, 3);
        sliderBatSize = new QSlider(centralWidget);
        sliderBatSize->setObjectName(QStringLiteral("sliderBatSize"));
        sliderBatSize->setMinimum(1);
        sliderBatSize->setMaximum(48);
        sliderBatSize->setValue(10);
        sliderBatSize->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(sliderBatSize);

        lblBatSize = new QLabel(centralWidget);
        lblBatSize->setObjectName(QStringLiteral("lblBatSize"));

        horizontalLayout_5->addWidget(lblBatSize, 0, Qt::AlignHCenter);

        horizontalLayout_5->setStretch(0, 5);
        horizontalLayout_5->setStretch(1, 2);

        gridLayout->addLayout(horizontalLayout_5, 9, 0, 1, 1);

        lblModuleName = new QLabel(centralWidget);
        lblModuleName->setObjectName(QStringLiteral("lblModuleName"));
        QFont font2;
        font2.setPointSize(10);
        lblModuleName->setFont(font2);

        gridLayout->addWidget(lblModuleName, 0, 0, 1, 1, Qt::AlignHCenter);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 3, -1, 3);
        labArr2 = new QLabel(centralWidget);
        labArr2->setObjectName(QStringLiteral("labArr2"));

        horizontalLayout_2->addWidget(labArr2, 0, Qt::AlignHCenter);

        fieldArr2 = new QLineEdit(centralWidget);
        fieldArr2->setObjectName(QStringLiteral("fieldArr2"));
        sizePolicy.setHeightForWidth(fieldArr2->sizePolicy().hasHeightForWidth());
        fieldArr2->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(fieldArr2);

        horizontalLayout_2->setStretch(0, 2);
        horizontalLayout_2->setStretch(1, 1);

        gridLayout->addLayout(horizontalLayout_2, 4, 0, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(1, 1);
        gridLayout->setRowStretch(2, 1);
        gridLayout->setRowStretch(3, 1);
        gridLayout->setRowStretch(4, 1);
        gridLayout->setRowStretch(5, 1);
        gridLayout->setRowStretch(6, 1);
        gridLayout->setRowStretch(7, 1);
        gridLayout->setRowStretch(8, 1);
        gridLayout->setRowStretch(9, 1);
        gridLayout->setRowStretch(10, 1);
        gridLayout->setRowStretch(11, 1);
        gridLayout->setRowStretch(12, 1);
        gridLayout->setRowStretch(13, 1);
        gridLayout->setRowStretch(14, 1);
        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 3);
        gridLayout->setColumnStretch(2, 1);
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PhD or Bust", Q_NULLPTR));
        menu_SamplePath->setText(QApplication::translate("MainWindow", "Sample Path", Q_NULLPTR));
        menu_SwitchedPoisson->setText(QApplication::translate("MainWindow", "Switched Poisson", Q_NULLPTR));
        rbProc3->setText(QApplication::translate("MainWindow", "Exponential", Q_NULLPTR));
        labProc3->setText(QApplication::translate("MainWindow", "Rate", Q_NULLPTR));
        lblVersion->setText(QApplication::translate("MainWindow", "CK Sim v0.2", Q_NULLPTR));
        rbProc1->setText(QApplication::translate("MainWindow", "Normal", Q_NULLPTR));
        labProc11->setText(QApplication::translate("MainWindow", "Mean", Q_NULLPTR));
        labProc12->setText(QApplication::translate("MainWindow", "Std. Dev.", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Arrival", Q_NULLPTR));
        lblHeader->setText(QApplication::translate("MainWindow", "Battery Simulator", Q_NULLPTR));
        lblWhartonLogo->setText(QApplication::translate("MainWindow", "Picture Placeholder", Q_NULLPTR));
        labArr1->setText(QApplication::translate("MainWindow", "Probability Production", Q_NULLPTR));
        lblNumPeriods->setText(QApplication::translate("MainWindow", "numPeriods", Q_NULLPTR));
        labArr3->setText(QApplication::translate("MainWindow", "Price Low", Q_NULLPTR));
        rbProc2->setText(QApplication::translate("MainWindow", "On/Off", Q_NULLPTR));
        labProc21->setText(QApplication::translate("MainWindow", "Pr[On]", Q_NULLPTR));
        labProc22->setText(QApplication::translate("MainWindow", "Rate", Q_NULLPTR));
        radioBtn10->setText(QApplication::translate("MainWindow", "10 hours", Q_NULLPTR));
        radioBtn12->setText(QApplication::translate("MainWindow", "12 hours", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Battery Size", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Inactive Parts", Q_NULLPTR));
        btnSimulate->setText(QApplication::translate("MainWindow", "Simulate", Q_NULLPTR));
        lblBatSize->setText(QApplication::translate("MainWindow", "BatSize", Q_NULLPTR));
        lblModuleName->setText(QApplication::translate("MainWindow", "Binomial Module", Q_NULLPTR));
        labArr2->setText(QApplication::translate("MainWindow", "Price High", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BINOMIALWINDOW_H

/********************************************************************************
** Form generated from reading UI file 'centerscreen.ui'
**
** Created by: Qt User Interface Compiler version 5.9.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CENTERSCREEN_H
#define UI_CENTERSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CenterScreen
{
public:
    QAction *menu_SamplePath;
    QAction *menu_SwitchedPoisson;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QPushButton *btnOption1;
    QPushButton *btnOption2;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *page_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *CenterScreen)
    {
        if (CenterScreen->objectName().isEmpty())
            CenterScreen->setObjectName(QStringLiteral("CenterScreen"));
        CenterScreen->resize(1152, 669);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CenterScreen->sizePolicy().hasHeightForWidth());
        CenterScreen->setSizePolicy(sizePolicy);
        menu_SamplePath = new QAction(CenterScreen);
        menu_SamplePath->setObjectName(QStringLiteral("menu_SamplePath"));
        menu_SwitchedPoisson = new QAction(CenterScreen);
        menu_SwitchedPoisson->setObjectName(QStringLiteral("menu_SwitchedPoisson"));
        centralwidget = new QWidget(CenterScreen);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        sizePolicy.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        btnOption1 = new QPushButton(centralwidget);
        btnOption1->setObjectName(QStringLiteral("btnOption1"));

        gridLayout->addWidget(btnOption1, 0, 1, 1, 1);

        btnOption2 = new QPushButton(centralwidget);
        btnOption2->setObjectName(QStringLiteral("btnOption2"));

        gridLayout->addWidget(btnOption2, 1, 1, 1, 1);

        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        sizePolicy.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        stackedWidget->addWidget(page_2);

        gridLayout->addWidget(stackedWidget, 1, 0, 1, 1);

        CenterScreen->setCentralWidget(centralwidget);
        menubar = new QMenuBar(CenterScreen);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1152, 21));
        CenterScreen->setMenuBar(menubar);
        statusbar = new QStatusBar(CenterScreen);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        CenterScreen->setStatusBar(statusbar);

        retranslateUi(CenterScreen);

        QMetaObject::connectSlotsByName(CenterScreen);
    } // setupUi

    void retranslateUi(QMainWindow *CenterScreen)
    {
        CenterScreen->setWindowTitle(QApplication::translate("CenterScreen", "MainWindow", Q_NULLPTR));
        menu_SamplePath->setText(QApplication::translate("CenterScreen", "Sample Path", Q_NULLPTR));
        menu_SwitchedPoisson->setText(QApplication::translate("CenterScreen", "Switched Poisson", Q_NULLPTR));
        btnOption1->setText(QApplication::translate("CenterScreen", "Sample Path", Q_NULLPTR));
        btnOption2->setText(QApplication::translate("CenterScreen", "Switched Poisson Process", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CenterScreen: public Ui_CenterScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CENTERSCREEN_H

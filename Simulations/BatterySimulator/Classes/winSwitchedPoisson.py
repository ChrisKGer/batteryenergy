import sys
from PyQt5 import QtCore, QtGui, QtWidgets

import numpy as np

from .matPltWidget import MatPltWidget

class WinSwitchedPoisson(QtWidgets.QTableWidget):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1152, 648)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralWidget)
        self.gridLayout.setContentsMargins(11, 11, 11, 11)
        self.gridLayout.setSpacing(6)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout_8.setSpacing(6)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.rbProc3 = QtWidgets.QRadioButton(self.centralWidget)
        self.rbProc3.setObjectName("rbProc3")
        self.horizontalLayout_8.addWidget(self.rbProc3)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setSpacing(6)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.labProc3 = QtWidgets.QLabel(self.centralWidget)
        self.labProc3.setObjectName("labProc3")
        self.horizontalLayout_10.addWidget(self.labProc3, 0, QtCore.Qt.AlignHCenter)
        self.fieldProc3 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldProc3.sizePolicy().hasHeightForWidth())
        self.fieldProc3.setSizePolicy(sizePolicy)
        self.fieldProc3.setObjectName("fieldProc3")
        self.horizontalLayout_10.addWidget(self.fieldProc3)
        self.horizontalLayout_10.setStretch(0, 1)
        self.horizontalLayout_10.setStretch(1, 1)
        self.horizontalLayout_8.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_8.setStretch(0, 4)
        self.horizontalLayout_8.setStretch(1, 5)
        self.gridLayout.addLayout(self.horizontalLayout_8, 14, 0, 1, 1)
        self.lblVersion = QtWidgets.QLabel(self.centralWidget)
        self.lblVersion.setObjectName("lblVersion")
        self.gridLayout.addWidget(self.lblVersion, 0, 2, 1, 1, QtCore.Qt.AlignRight | QtCore.Qt.AlignTop)
        self.StatePlot = QtWidgets.QWidget(self.centralWidget)
        self.StatePlot.setObjectName("StatePlot")
        self.gridLayout.addWidget(self.StatePlot, 3, 2, 12, 1)
        self.SamplePathPlot = QtWidgets.QWidget(self.centralWidget)
        self.SamplePathPlot.setObjectName("SamplePathPlot")
        self.gridLayout.addWidget(self.SamplePathPlot, 3, 1, 12, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout_6.setSpacing(6)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.rbProc1 = QtWidgets.QRadioButton(self.centralWidget)
        self.rbProc1.setObjectName("rbProc1")
        self.horizontalLayout_6.addWidget(self.rbProc1)
        self.gridLayout_5 = QtWidgets.QGridLayout()
        self.gridLayout_5.setSpacing(6)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.labProc11 = QtWidgets.QLabel(self.centralWidget)
        self.labProc11.setObjectName("labProc11")
        self.gridLayout_5.addWidget(self.labProc11, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.labProc12 = QtWidgets.QLabel(self.centralWidget)
        self.labProc12.setObjectName("labProc12")
        self.gridLayout_5.addWidget(self.labProc12, 1, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.fieldProc11 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldProc11.sizePolicy().hasHeightForWidth())
        self.fieldProc11.setSizePolicy(sizePolicy)
        self.fieldProc11.setObjectName("fieldProc11")
        self.gridLayout_5.addWidget(self.fieldProc11, 0, 1, 1, 1)
        self.fieldProc12 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldProc12.sizePolicy().hasHeightForWidth())
        self.fieldProc12.setSizePolicy(sizePolicy)
        self.fieldProc12.setObjectName("fieldProc12")
        self.gridLayout_5.addWidget(self.fieldProc12, 1, 1, 1, 1)
        self.gridLayout_5.setColumnStretch(0, 1)
        self.gridLayout_5.setColumnStretch(1, 1)
        self.horizontalLayout_6.addLayout(self.gridLayout_5)
        self.horizontalLayout_6.setStretch(0, 4)
        self.horizontalLayout_6.setStretch(1, 5)
        self.gridLayout.addLayout(self.horizontalLayout_6, 12, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 2, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.line_2 = QtWidgets.QFrame(self.centralWidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 10, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.centralWidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 7, 0, 1, 1)
        self.lblHeader = QtWidgets.QLabel(self.centralWidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.lblHeader.setFont(font)
        self.lblHeader.setObjectName("lblHeader")
        self.gridLayout.addWidget(self.lblHeader, 0, 1, 1, 1, QtCore.Qt.AlignHCenter)
        self.lblWhartonLogo = QtWidgets.QLabel(self.centralWidget)
        self.lblWhartonLogo.setObjectName("lblWhartonLogo")
        self.gridLayout.addWidget(self.lblWhartonLogo, 1, 2, 2, 1, QtCore.Qt.AlignHCenter)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.labArr1 = QtWidgets.QLabel(self.centralWidget)
        self.labArr1.setObjectName("labArr1")
        self.horizontalLayout.addWidget(self.labArr1, 0, QtCore.Qt.AlignHCenter)
        self.fieldArr1 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldArr1.sizePolicy().hasHeightForWidth())
        self.fieldArr1.setSizePolicy(sizePolicy)
        self.fieldArr1.setObjectName("fieldArr1")
        self.horizontalLayout.addWidget(self.fieldArr1)
        self.horizontalLayout.setStretch(0, 2)
        self.horizontalLayout.setStretch(1, 1)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setSpacing(6)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.sliderPeriods = QtWidgets.QSlider(self.centralWidget)
        self.sliderPeriods.setMinimum(1)
        self.sliderPeriods.setMaximum(1000)
        self.sliderPeriods.setProperty("value", 100)
        self.sliderPeriods.setOrientation(QtCore.Qt.Horizontal)
        self.sliderPeriods.setObjectName("sliderPeriods")
        self.horizontalLayout_11.addWidget(self.sliderPeriods)
        self.lblNumPeriods = QtWidgets.QLabel(self.centralWidget)
        self.lblNumPeriods.setObjectName("lblNumPeriods")
        self.horizontalLayout_11.addWidget(self.lblNumPeriods, 0, QtCore.Qt.AlignHCenter)
        self.horizontalLayout_11.setStretch(0, 5)
        self.horizontalLayout_11.setStretch(1, 1)
        self.gridLayout.addLayout(self.horizontalLayout_11, 1, 1, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.labArr3 = QtWidgets.QLabel(self.centralWidget)
        self.labArr3.setObjectName("labArr3")
        self.horizontalLayout_3.addWidget(self.labArr3, 0, QtCore.Qt.AlignHCenter)
        self.fieldArr3 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldArr3.sizePolicy().hasHeightForWidth())
        self.fieldArr3.setSizePolicy(sizePolicy)
        self.fieldArr3.setObjectName("fieldArr3")
        self.horizontalLayout_3.addWidget(self.fieldArr3)
        self.horizontalLayout_3.setStretch(0, 2)
        self.horizontalLayout_3.setStretch(1, 1)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 1)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout_7.setSpacing(6)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.rbProc2 = QtWidgets.QRadioButton(self.centralWidget)
        self.rbProc2.setObjectName("rbProc2")
        self.horizontalLayout_7.addWidget(self.rbProc2)
        self.gridLayout_6 = QtWidgets.QGridLayout()
        self.gridLayout_6.setSpacing(6)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.fieldProc22 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldProc22.sizePolicy().hasHeightForWidth())
        self.fieldProc22.setSizePolicy(sizePolicy)
        self.fieldProc22.setObjectName("fieldProc22")
        self.gridLayout_6.addWidget(self.fieldProc22, 1, 1, 1, 1)
        self.fieldProc21 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldProc21.sizePolicy().hasHeightForWidth())
        self.fieldProc21.setSizePolicy(sizePolicy)
        self.fieldProc21.setObjectName("fieldProc21")
        self.gridLayout_6.addWidget(self.fieldProc21, 0, 1, 1, 1)
        self.labProc21 = QtWidgets.QLabel(self.centralWidget)
        self.labProc21.setObjectName("labProc21")
        self.gridLayout_6.addWidget(self.labProc21, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.labProc22 = QtWidgets.QLabel(self.centralWidget)
        self.labProc22.setObjectName("labProc22")
        self.gridLayout_6.addWidget(self.labProc22, 1, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.gridLayout_6.setColumnStretch(0, 1)
        self.gridLayout_6.setColumnStretch(1, 1)
        self.horizontalLayout_7.addLayout(self.gridLayout_6)
        self.horizontalLayout_7.setStretch(0, 4)
        self.horizontalLayout_7.setStretch(1, 5)
        self.gridLayout.addLayout(self.horizontalLayout_7, 13, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.labArr4 = QtWidgets.QLabel(self.centralWidget)
        self.labArr4.setObjectName("labArr4")
        self.horizontalLayout_4.addWidget(self.labArr4, 0, QtCore.Qt.AlignHCenter)
        self.fieldArr4 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldArr4.sizePolicy().hasHeightForWidth())
        self.fieldArr4.setSizePolicy(sizePolicy)
        self.fieldArr4.setObjectName("fieldArr4")
        self.horizontalLayout_4.addWidget(self.fieldArr4)
        self.horizontalLayout_4.setStretch(0, 2)
        self.horizontalLayout_4.setStretch(1, 1)
        self.gridLayout.addLayout(self.horizontalLayout_4, 6, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.centralWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 8, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.label_5 = QtWidgets.QLabel(self.centralWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 11, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.btnSimulate = QtWidgets.QPushButton(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnSimulate.sizePolicy().hasHeightForWidth())
        self.btnSimulate.setSizePolicy(sizePolicy)
        self.btnSimulate.setToolTipDuration(1)
        self.btnSimulate.setObjectName("btnSimulate")
        self.gridLayout.addWidget(self.btnSimulate, 2, 1, 1, 1)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setContentsMargins(10, 3, -1, 3)
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.sliderBatSize = QtWidgets.QSlider(self.centralWidget)
        self.sliderBatSize.setMinimum(1)
        self.sliderBatSize.setMaximum(48)
        self.sliderBatSize.setProperty("value", 10)
        self.sliderBatSize.setOrientation(QtCore.Qt.Horizontal)
        self.sliderBatSize.setObjectName("sliderBatSize")
        self.horizontalLayout_5.addWidget(self.sliderBatSize)
        self.lblBatSize = QtWidgets.QLabel(self.centralWidget)
        self.lblBatSize.setObjectName("lblBatSize")
        self.horizontalLayout_5.addWidget(self.lblBatSize, 0, QtCore.Qt.AlignHCenter)
        self.horizontalLayout_5.setStretch(0, 5)
        self.horizontalLayout_5.setStretch(1, 2)
        self.gridLayout.addLayout(self.horizontalLayout_5, 9, 0, 1, 1)
        self.lblModuleName = QtWidgets.QLabel(self.centralWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lblModuleName.setFont(font)
        self.lblModuleName.setObjectName("lblModuleName")
        self.gridLayout.addWidget(self.lblModuleName, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 3, -1, 3)
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.labArr2 = QtWidgets.QLabel(self.centralWidget)
        self.labArr2.setObjectName("labArr2")
        self.horizontalLayout_2.addWidget(self.labArr2, 0, QtCore.Qt.AlignHCenter)
        self.fieldArr2 = QtWidgets.QLineEdit(self.centralWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fieldArr2.sizePolicy().hasHeightForWidth())
        self.fieldArr2.setSizePolicy(sizePolicy)
        self.fieldArr2.setObjectName("fieldArr2")
        self.horizontalLayout_2.addWidget(self.fieldArr2)
        self.horizontalLayout_2.setStretch(0, 2)
        self.horizontalLayout_2.setStretch(1, 1)
        self.gridLayout.addLayout(self.horizontalLayout_2, 4, 0, 1, 1)
        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 3)
        self.gridLayout.setColumnStretch(2, 1)
        self.gridLayout.setRowStretch(0, 1)
        self.gridLayout.setRowStretch(1, 1)
        self.gridLayout.setRowStretch(2, 1)
        self.gridLayout.setRowStretch(3, 1)
        self.gridLayout.setRowStretch(4, 1)
        self.gridLayout.setRowStretch(5, 1)
        self.gridLayout.setRowStretch(6, 1)
        self.gridLayout.setRowStretch(7, 1)
        self.gridLayout.setRowStretch(8, 1)
        self.gridLayout.setRowStretch(9, 1)
        self.gridLayout.setRowStretch(10, 1)
        self.gridLayout.setRowStretch(11, 1)
        self.gridLayout.setRowStretch(12, 1)
        self.gridLayout.setRowStretch(13, 1)
        self.gridLayout.setRowStretch(14, 1)
        MainWindow.setCentralWidget(self.centralWidget)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)
        self.menu_SamplePath = QtWidgets.QAction(MainWindow)
        self.menu_SamplePath.setObjectName("menu_SamplePath")
        self.menu_SwitchedPoisson = QtWidgets.QAction(MainWindow)
        self.menu_SwitchedPoisson.setObjectName("menu_SwitchedPoisson")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        ####### Own Code
        #Matplot stuff
        self.SamplePathPlot = MatPltWidget(self)
        self.SamplePathPlot.setObjectName("SamplePathPlot")
        self.gridLayout.addWidget(self.SamplePathPlot, 3, 1, 13, 1)
        self.StatePlot = MatPltWidget(self)
        self.StatePlot.setObjectName("StatePlot")
        self.gridLayout.addWidget(self.StatePlot, 3, 2, 13, 1)


        #All other things
        self.add_Functionality(MainWindow)
        self.load_Defaults(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PhD or Bust"))
        self.rbProc3.setText(_translate("MainWindow", "Exponential"))
        self.labProc3.setText(_translate("MainWindow", "Rate"))
        self.lblVersion.setText(_translate("MainWindow", "CK Sim v0.2"))
        self.rbProc1.setText(_translate("MainWindow", "Normal"))
        self.labProc11.setText(_translate("MainWindow", "Mean"))
        self.labProc12.setText(_translate("MainWindow", "Std. Dev."))
        self.label_4.setText(_translate("MainWindow", "Arrival Rate"))
        self.lblHeader.setText(_translate("MainWindow", "Battery Simulator"))
        self.lblWhartonLogo.setText(_translate("MainWindow", "Picture Placeholder"))
        self.labArr1.setText(_translate("MainWindow", "Rate Arrival1"))
        self.lblNumPeriods.setText(_translate("MainWindow", "numPeriods"))
        self.labArr3.setText(_translate("MainWindow", "Rate Transition 1-2"))
        self.rbProc2.setText(_translate("MainWindow", "On/Off"))
        self.labProc21.setText(_translate("MainWindow", "Pr[On]"))
        self.labProc22.setText(_translate("MainWindow", "Rate"))
        self.labArr4.setText(_translate("MainWindow", "Rate Transition 2-1"))
        self.label_6.setText(_translate("MainWindow", "Battery Size"))
        self.label_5.setText(_translate("MainWindow", "Processing Rate"))
        self.btnSimulate.setText(_translate("MainWindow", "Simulate"))
        self.lblBatSize.setText(_translate("MainWindow", "BatSize"))
        self.lblModuleName.setText(_translate("MainWindow", "Switched Poisson Process Module"))
        self.labArr2.setText(_translate("MainWindow", "Rate Arrival2"))
        self.menu_SamplePath.setText(_translate("MainWindow", "Sample Path"))
        self.menu_SwitchedPoisson.setText(_translate("MainWindow", "Switched Poisson"))

    def add_Functionality(self, MainWindow):
        #Group Radtio buttons
        self.rbGroupProc = QtWidgets.QButtonGroup()
        self.rbGroupProc.addButton(self.rbProc1)
        self.rbGroupProc.addButton(self.rbProc2)
        self.rbGroupProc.addButton(self.rbProc3)

        #Functionality to store slider value
        self.sliderPeriods.valueChanged.connect(lambda: self.set_numPeriods(self.sliderPeriods.value()))
        self.sliderBatSize.valueChanged.connect(lambda: self.set_batSize(self.sliderBatSize.value()))

        self.varProcSel =  1
        self.rbProc1.clicked.connect(lambda: self.set_varProc(1))
        self.rbProc2.clicked.connect(lambda: self.set_varProc(2))
        self.rbProc3.clicked.connect(lambda: self.set_varProc(3))

        self.btnSimulate.clicked.connect(lambda: self.simulate())


    def load_Defaults(self, MainWindow):
        #Set Icon
        MainWindow.setWindowIcon(QtGui.QIcon("battery_icon.ico"))

        #Set Current Num Periods
        self.lblNumPeriods.setText(str(self.sliderPeriods.value()))
        self.set_numPeriods(self.sliderPeriods.value())
        self.set_batSize(self.sliderBatSize.value())

        #Set Arrival Defaults
        self.rbProc2.setChecked(True)
        self.set_varProc(2)
        self.fieldArr1.setText("0")
        self.fieldArr2.setText(".5")
        self.fieldArr3.setText("4")
        self.fieldArr4.setText("4")
        self.fieldProc11.setText("0")
        self.fieldProc12.setText("1")
        self.fieldProc21.setText("0.25")
        self.fieldProc22.setText("0.25")
        self.fieldProc3.setText("1")


###### Actual functions
    def calc_Sample_Path(self, lam, mu, n, batSize):
        numArr = lam.shape[0]
        numDep = mu.shape[0]
        path = np.zeros(shape=(numArr + numDep, 3))
        path[0:numArr, 0] = lam.ravel()
        path[0:numArr, 1] = +1
        path[numArr:(numArr + numDep), 0] = mu.ravel()
        path[numArr:(numArr + numDep), 1] = -1
        path = path[path[:, 0].argsort()]
        path[0, 2] = max(0, path[0, 1])  # start of sample path
        for i in range(numArr + numDep - 1):
            path[i + 1, 2] = min(max(0, path[i, 2] + path[i + 1, 1]),batSize)
        path = path[path[:, 0] <= n, :]
        return path[:, 0], path[:, 2]

    def calc_State_Plot(self, y):
        maxObs = y.max()
        bins = maxObs
        y2 = y
        if maxObs > 20:
            cutoff = np.percentile(y, 95)
            y2 = np.clip(y,-1,cutoff)
            bins = 20

        return y2, int(bins)

    def simulate(self):

        n = self.varNumPeriods
        t = 0
        rate1 = float(self.fieldArr1.text())
        if rate1 == 0:
            rate1 = 9999
        rate2 = float(self.fieldArr2.text())
        trans1 = float(self.fieldArr3.text())
        trans2 = float(self.fieldArr4.text())
        state = 1
        arrivals = list()
        self.states = list()
        #self.states.append(state)
        self.statestime = list()
        #self.statestime.append(t)

        while t < n:
            if state == 1:
                a = np.random.exponential(scale = rate1)
                b = np.random.exponential(scale = trans1)
                if a < b:
                    t += a
                    arrivals.append(t)
                else:
                    t += b
                    self.states.append(state)
                    state = 2
                    self.statestime.append(t)
            if state == 2:
                a = np.random.exponential(scale = rate2)
                b = np.random.exponential(scale = trans2)
                if a < b:
                    t += a
                    arrivals.append(t)
                else:
                    t += b
                    self.states.append(state)
                    state = 1
                    self.statestime.append(t)

        self.lamArr = arrivals

        if self.varProcSel == 1:
            self.muArr = np.random.normal(loc=float(self.fieldProc11.text()), scale=float(self.fieldProc12.text()),
                                          size=int(((2 * n) / (float(self.fieldProc11.text()) + 0.1) + (2 * n)) / (float(self.fieldProc12.text())+0.1))).reshape(-1, 1)
            self.muArr = self.muArr * (self.muArr > 0)

        elif self.varProcSel == 2:
            departures = list()
            t = 0
            Pr = float(self.fieldProc21.text())
            rateDep = float(self.fieldProc22.text())
            numBulkDep = int(1/rateDep)
            #Calculate the bulk departure style process
            while t<n:
                if np.random.uniform()<Pr:
                    for i in range(numBulkDep):
                        t += np.random.exponential(scale=rateDep)
                        departures.append(t)
                else:
                    for i in range(numBulkDep):
                        t += np.random.exponential(scale=rateDep)

            self.muArr = np.diff(departures)

        elif self.varProcSel == 3:
            self.muArr = np.random.exponential(scale=float(self.fieldProc3.text()),
                                               size=int((2 * n) / (float(self.fieldProc3.text()) + 0.1))).reshape(-1, 1)


        self.lamArr = np.array(self.lamArr).reshape(-1, 1)  # -1 is unspecified and just fills to how ever many rows we have
        self.muArr = np.array(np.cumsum(self.muArr)).reshape(-1, 1)
        self.x, self.y = self.calc_Sample_Path(self.lamArr, self.muArr, n, self.varBatSize)
        self.y2, self.bins = self.calc_State_Plot(self.y)

        self.SamplePathPlot.axis.clear()
        self.SamplePathPlot.axis.step(self.x,self.y)
        self.SamplePathPlot.axis.step(self.statestime,self.states)
        self.SamplePathPlot.canvas.draw()

        self.StatePlot.axis.clear()
        self.StatePlot.axis.hist(self.y2, bins=self.bins)
        self.StatePlot.canvas.draw()



    def set_varArr(self, value):
        self.varArrSel = value

    def set_varProc(self, value):
        self.varProcSel = value

    def set_numPeriods(self, value):
        self.varNumPeriods = value
        self.lblNumPeriods.setText(str(value))

    def set_batSize(self, value):
        self.varBatSize = value
        self.lblBatSize.setText(str(value))
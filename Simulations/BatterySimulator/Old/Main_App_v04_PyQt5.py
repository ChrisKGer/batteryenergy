from PyQt5 import QtCore, QtGui, QtWidgets
import sys


class Window(QMainWindow):
    #Core Window stuff
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(100,100,1000,700)
        self.setWindowTitle("PhD or Bust")
        self.setWindowIcon(QtGui.QIcon("battery_icon.ico"))
        self.statusBar() # At the bottom

        #Main Menu
        mainMenu = self.menuBar()

        #First Category
        fileMenu = mainMenu.addMenu("&File")

        #First Option
        extractAction = QAction("Close App", self)
        extractAction.setShortcut("Ctrl+W")
        extractAction.setStatusTip("Exit Window")
        extractAction.triggered.connect(self.close_application)
        fileMenu.addAction(extractAction)




        self.home()


    def close_application(self):
        choice = QMessageBox.question(self, "Extract", "Get into the Chopper", QMessageBox.Yes | QMessageBox.No)
        if choice == QMessageBox.Yes:
            print("Extraction NOW")
            sys.exit()

    def download(self):
        self.completed = 0

        while self.completed <100:
            self.completed += 0.0001
            self.progress.setValue(self.completed)

    def enlarge_window(self, state):
        if state == QtCore.Qt.Checked:
            self.setGeometry(460,100,1000,900)
        else:
            self.setGeometry(460,100,1000,700)

    def font_choice(self):
        font, valid = QFontDialog.getFont()
        if valid:
            self.styleChoice.setFont(font)

    def home(self):
        btn = QPushButton("Simulate", self)
        btn.clicked.connect(self.close_application)
        btn.resize(btn.sizeHint()) #or minimumSizeHint
        btn.move(125,125)

        extractAction = QAction(QtGui.QIcon("Choppa.png"), "Go 2 Sim", self)
        extractAction.triggered.connect(self.close_application)
        self.toolBar = self.addToolBar('Extraction')
        self.toolBar.addAction(extractAction)

        fontChoice = QAction("Font", self)
        fontChoice.triggered.connect(self.font_choice)
        self.toolBar = self.addToolBar('Font')
        self.toolBar.addAction(fontChoice)


        checkBox = QCheckBox("Enlarge Window", self)
        checkBox.move(200, 200)
        checkBox.stateChanged.connect(self.enlarge_window)

        self.progress = QProgressBar(self)
        self.progress.setGeometry(200,80,250,20)

        self.btn = QPushButton("Download", self)
        self.btn.move(200,100)
        self.btn.clicked.connect(self.download)

        print(self.style().objectName())
        self.styleChoice = QLabel("Windows", self)

        comboBox = QComboBox(self)
        comboBox.addItem("motif")
        comboBox.addItem("Windows")
        comboBox.move(300,500)
        self.styleChoice.move(300,400)
        comboBox.activated[str].connect(self.style_choice)



        self.show()

    def style_choice(self, text):
        self.styleChoice.setText(text)
        QApplication.setStyle(QStyleFactory.create(text))

def run():
    app = QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())

run()

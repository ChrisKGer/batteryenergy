import tkinter as tk
from tkinter import ttk
import matplotlib
matplotlib.use("TkAgg") #make sure you use the tkinter backend
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib import pyplot as plt
#from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib import style
import numpy as np
import pandas as pd

fBattery = plt.figure()
aBattery1 = plt.subplot2grid((10, 7), (0, 0), rowspan=10, colspan=5)

#Define constants and global variables and styles
style.use("ggplot")
LARGE_FONT = {"Verdana", 12}
NORMAL_FONT = {"Verdana", 10}
SMALL_FONT = {"Verdana", 8}
gSimulFlag = 0
gArrChoice = 0
gServChoice = 0
gNumPeriods = 0
gArr1 = gArr21 = gArr22 = gArr31 = gArr32 = gArr41 = gArr42 = 0
gProc1 = gProc21 = gProc22 = gProc31 = gProc32 = gProc41 = gProc42 = 0

class BatSimGUI(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs) #Start up the tk.TK class

        #Overall properties
        tk.Tk.iconbitmap(self, default="battery_icon.ico") #sets icon top left
        tk.Tk.wm_title(self, "PhD or Bust")

        container = tk.Frame(self) #This is where everything lives
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1) #Defines row minimum height and importance
        container.grid_columnconfigure(0, weight=1) #Defines column minimum height and importance

        menubar = tk.Menu(container) #Add the menubar at the top of screen
        filemenu = tk.Menu(menubar, tearoff=0) #Create object that will become the first menu choice
        filemenu.add_command(label="Save settings", command = lambda: popupmsg("Not supported just yet!")) #Add first field to first menu choice
        filemenu.add_separator() #Add seperator
        filemenu.add_command(label="Exit", command=quit)
        menubar.add_cascade(label="File", menu=filemenu) #Add File to menubar

        tk.Tk.config(self, menu=menubar)
        fBattery = plt.figure()


        self.frames = {} #stores all my frames

        for F in (StartPage, BatteryPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew") #where to place this frame on the grid and how to align it

        self.show_frame(BatteryPage) # What to show upon initialization

        print("End of Initialization")

    def show_frame(self, cont): #bring to the the front, whatever frame with the key passed into cont is there
        frame = self.frames[cont]
        frame.tkraise()

def popupmsg(msg):
    popup = tk.Tk()
    popup.wm_title("!")
    label = ttk.Label(popup, text=msg, font=NORMAL_FONT)
    label.pack(side="top", fill="x",  pady=10)
    b1 = ttk.Button(popup, text="Okay", command = popup.destroy)
    b1.pack()
    popup.mainloop()

class StartPage(tk.Frame):

    def __init__(self, parent, controller): #initializes a new class in a container
        tk.Frame.__init__(self, parent)

        label = ttk.Label(self, text="Use Simulator at own risk", font = LARGE_FONT) # create a label
        label.pack(padx=10, pady=10) #add the label to the window

        button1 = ttk.Button(self, text="Battery Graph Page",
                             command=lambda: controller.show_frame(BatteryPage)) #needs the lambda, or doesnt work properly
        button1.pack()

class BatteryPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        #Set defaults for Distribution values
        global gArrChoice
        global gServChoice
        global gNumPeriods
        global gArr1, gArr21, gArr22, gArr31, gArr32, gArr41, gArr42
        global gProc1, gProc21, gProc22, gProc31, gProc32, gProc41, gProc42
        gArrChoice = tk.IntVar()
        gArrChoice.set(1)
        gServChoice = tk.IntVar()
        gServChoice.set(1)
        gNumPeriods = tk.DoubleVar()
        gNumPeriods.set(100)
        gArr1 = tk.DoubleVar()
        gArr1.set(1.0)
        gArr21 = tk.DoubleVar()
        gArr21.set(0.0)
        gArr22 = tk.DoubleVar()
        gArr22.set(1.0)
        gArr31 = tk.DoubleVar()
        gArr31.set(0.0)
        gArr32 = tk.DoubleVar()
        gArr32.set(1.0)
        gArr41 = tk.DoubleVar()
        gArr41.set(1.0)
        gArr42 = tk.DoubleVar()
        gArr42.set(3)

        gProc1 = tk.DoubleVar()
        gProc1.set(1.0)
        gProc21 = tk.DoubleVar()
        gProc21.set(0.0)
        gProc22 = tk.DoubleVar()
        gProc22.set(1.0)
        gProc31 = tk.DoubleVar()
        gProc31.set(0.0)
        gProc32 = tk.DoubleVar()
        gProc32.set(1.0)
        gProc41 = tk.DoubleVar()
        gProc41.set(1.0)
        gProc42 = tk.DoubleVar()
        gProc42.set(3)

        self.grid_rowconfigure(0, weight=1, minsize=50) #Defines row minimum height and importance
        self.grid_columnconfigure(0, weight=1) #Defines column minimum height and importance
        self.columnconfigure(0, weight=1, uniform='rateColumn')
        self.columnconfigure(1, weight=1, uniform='rateRadioButton')
        self.columnconfigure(7, weight=1, uniform='rateRadioButton')
        self.columnconfigure(8, weight=1, uniform='rateRadioButton')

        self.rowconfigure(0, weight=0, pad = 10) #Collapse that top row
        #for r in range(9):
        #    self.rowconfigure(r+1, weight=1, uniform="default row")  # Collapse that top row

        #Top Center
        labelTitle = ttk.Label(self, text="Battery Page", font=LARGE_FONT)  # create a label
        labelTitle.grid(row = 0, column=2, columnspan=5, sticky="N")  # add the label to the window

        buttonSimul = ttk.Button(self, text="Simulate",
                             command= lambda: simulStart()) #live graph functionality)  # needs the lambda, or doesnt work properly
        buttonSimul.grid(row=2, column=5, sticky="NSEW")

        labelPeriod = ttk.Label(self, text="# Periods", font=NORMAL_FONT)
        labelPeriod.grid(row=1, column=2, sticky="N")

        labelNumPeriod = ttk.Label(self, text=gNumPeriods.get(), font=NORMAL_FONT)
        labelNumPeriod .grid(row=1, column=6, sticky="W")

        scalePeriod = ttk.Scale(self, from_=2, to = 1000, orient="horizontal", variable= gNumPeriods,
                                command= lambda _: labelNumPeriod.configure(text=gNumPeriods.get()))
        scalePeriod.grid(row=1,column=3, columnspan=3, sticky="NSEW")

        # Arrival Rate
        labelArrival = ttk.Label(self, text="Arrival Rate", font=NORMAL_FONT)  # create a label
        labelArrival.grid(row=1, column=0, columnspan=2, sticky="N")  # add the label to the window

        radioArr1 = ttk.Radiobutton(self, variable=gArrChoice, text="Exponential", value=1,
                                 command= lambda: gArrChoice.set(1))
        radioArr1.grid(row=2, column=0, sticky="W", padx = 10)
        radioArr2 = ttk.Radiobutton(self, variable=gArrChoice, text="Normal", value=2,
                                 command= lambda: gArrChoice.set(2))
        radioArr2.grid(row=3, column=0, sticky="W", padx = 10)
        radioArr3 = ttk.Radiobutton(self, variable=gArrChoice, text="Uniform", value=3,
                                 command= lambda: gArrChoice.set(3))
        radioArr3.grid(row=4, column=0, sticky="W", padx = 10)
        radioArr4 = ttk.Radiobutton(self, variable=gArrChoice, text="Erlang", value=4,
                                 command= lambda: gArrChoice.set(4))
        radioArr4.grid(row=5, column=0, sticky="W", padx = 10)


        #Create arrival rate frames and then put rate and label in frame
        frameArr1 = tk.Frame(self)
        frameArr1.grid(row=2, column=1, sticky="W")
        entryArr1 = ttk.Entry(frameArr1, width=5, textvariable=gArr1)
        entryArr1.grid(row=0, column=0, sticky="W")
        labelArr1= ttk.Label(frameArr1, text=u"\u03bb", font=SMALL_FONT)  # create a label
        labelArr1.grid(row = 0, column=1, sticky="W", padx = 5)

        frameArr2 = tk.Frame(self)
        frameArr2.grid(row=3, column=1, sticky="W")
        entryArr21 = ttk.Entry(frameArr2, width=5, textvariable=gArr21)
        entryArr21.grid(row=0, column=0, sticky="W")
        labelArr21= ttk.Label(frameArr2, text=u"\u03bc", font=SMALL_FONT)  # mu
        labelArr21.grid(row = 0, column=1, sticky="W", padx = 5)
        entryArr22 = ttk.Entry(frameArr2, width=5, textvariable=gArr22)
        entryArr22.grid(row=0, column=2, sticky="W")
        labelArr22= ttk.Label(frameArr2, text=u"\u03c3", font=SMALL_FONT)  # sigma
        labelArr22.grid(row = 0, column=3, sticky="W", padx = 5)

        frameArr3 = tk.Frame(self)
        frameArr3.grid(row=4, column=1, sticky="W")
        entryArr31 = ttk.Entry(frameArr3, width=5, textvariable=gArr31)
        entryArr31.grid(row=0, column=0, sticky="W")
        labelArr31= ttk.Label(frameArr3, text="LL", font=SMALL_FONT)  # mu
        labelArr31.grid(row = 0, column=1, sticky="W", padx = 5)
        entryArr32 = ttk.Entry(frameArr3, width=5, textvariable=gArr32)
        entryArr32.grid(row=0, column=2, sticky="W")
        labelArr32= ttk.Label(frameArr3, text="UL", font=SMALL_FONT)  # sigma
        labelArr32.grid(row = 0, column=3, sticky="W", padx = 5)

        frameArr4 = tk.Frame(self)
        frameArr4.grid(row=5, column=1, sticky="W")
        entryArr41 = ttk.Entry(frameArr4, width=5, textvariable=gArr41)
        entryArr41.grid(row=0, column=0, sticky="W")
        labelArr41= ttk.Label(frameArr4, text=u"\u03bb", font=SMALL_FONT)  # mu
        labelArr41.grid(row = 0, column=1, sticky="W", padx = 5)
        entryArr42 = ttk.Entry(frameArr4, width=5, textvariable=gArr42)
        entryArr42.grid(row=0, column=2, sticky="W")
        labelArr42= ttk.Label(frameArr4, text="k", font=SMALL_FONT)  # sigma
        labelArr42.grid(row = 0, column=3, sticky="W", padx = 5)

        # Processing Rate
        labelProcessing = ttk.Label(self, text="Processing Rate", font=NORMAL_FONT)  # create a label
        labelProcessing.grid(row=6, column=0, columnspan=2, sticky="N")  # add the label to the window

        radioProc1 = ttk.Radiobutton(self, variable=gServChoice, text="Exponential", value=1,
                                 command= lambda: gServChoice.set(1))
        radioProc1.grid(row=7, column=0, sticky="W", padx = 10)
        radioProc2 = ttk.Radiobutton(self, variable=gServChoice, text="Normal", value=2,
                                 command= lambda: gServChoice.set(2))
        radioProc2.grid(row=8, column=0, sticky="W", padx = 10)
        radioProc3 = ttk.Radiobutton(self, variable=gServChoice, text="Uniform", value=3,
                                 command= lambda: gServChoice.set(3))
        radioProc3.grid(row=9, column=0, sticky="W", padx = 10)
        radioProc4 = ttk.Radiobutton(self, variable=gServChoice, text="Erlang", value=4,
                                 command= lambda: gServChoice.set(4))
        radioProc4.grid(row=10, column=0, sticky="W", padx = 10)

        #Create frame to then put in the rate and input box
        frameProc1 = tk.Frame(self)
        frameProc1.grid(row=7, column=1, sticky="W")
        entryProc1 = ttk.Entry(frameProc1, width=5, textvariable=gProc1)
        entryProc1.grid(row=0, column=0, sticky="W")
        labelProc1= ttk.Label(frameProc1, text=u"\u03bb", font=SMALL_FONT)  # create a label
        labelProc1.grid(row = 0, column=1, sticky="W", padx = 5)

        frameProc2 = tk.Frame(self)
        frameProc2.grid(row=8, column=1, sticky="W")
        entryProc21 = ttk.Entry(frameProc2, width=5, textvariable=gProc21)
        entryProc21.grid(row=0, column=0, sticky="W")
        labelProc21= ttk.Label(frameProc2, text=u"\u03bc", font=SMALL_FONT)  # mu
        labelProc21.grid(row = 0, column=1, sticky="W", padx = 5)
        entryProc22 = ttk.Entry(frameProc2, width=5, textvariable=gProc22)
        entryProc22.grid(row=0, column=2, sticky="W")
        labelProc22= ttk.Label(frameProc2, text=u"\u03c3", font=SMALL_FONT)  # sigma
        labelProc22.grid(row = 0, column=3, sticky="W", padx = 5)

        frameProc3 = tk.Frame(self)
        frameProc3.grid(row=9, column=1, sticky="W")
        entryProc31 = ttk.Entry(frameProc3, width=5, textvariable=gProc31)
        entryProc31.grid(row=0, column=0, sticky="W")
        labelProc31= ttk.Label(frameProc3, text="LL", font=SMALL_FONT)  # mu
        labelProc31.grid(row = 0, column=1, sticky="W", padx = 5)
        entryProc32 = ttk.Entry(frameProc3, width=5, textvariable=gProc32)
        entryProc32.grid(row=0, column=2, sticky="W")
        labelProc32= ttk.Label(frameProc3, text="UL", font=SMALL_FONT)  # sigma
        labelProc32.grid(row = 0, column=3, sticky="W", padx = 5)

        frameProc4 = tk.Frame(self)
        frameProc4.grid(row=10, column=1, sticky="W")
        entryProc41 = ttk.Entry(frameProc4, width=5, textvariable=gProc41)
        entryProc41.grid(row=0, column=0, sticky="W")
        labelProc41= ttk.Label(frameProc4, text=u"\u03bb", font=SMALL_FONT)  # mu
        labelProc41.grid(row = 0, column=1, sticky="W", padx = 5)
        entryProc42 = ttk.Entry(frameProc4, width=5, textvariable=gProc42)
        entryProc42.grid(row=0, column=2, sticky="W")
        labelProc42= ttk.Label(frameProc4, text="k", font=SMALL_FONT)  # sigma
        labelProc42.grid(row = 0, column=3, sticky="W", padx = 5)

        #Add Graph
        canvas = FigureCanvasTkAgg(fBattery, self) #transforms figure into tkinter thing
        canvas.draw() #Show the plot
        canvas.get_tk_widget().grid(row=3, column=2, columnspan=5, rowspan = 10, sticky="NSEW") #Add the plot to the graph

        toolbarFrame = tk.Frame(self)
        toolbarFrame.grid(row=13, column=2, columnspan=5, sticky="S")
        toolbar = NavigationToolbar2Tk(canvas, toolbarFrame)
        toolbar.update()

        #Back Button Stuff
        buttonBack = ttk.Button(self, text="Back To Home",
                            command=lambda: controller.show_frame(StartPage))  # needs the lambda, or doesnt work properly
        buttonBack.grid(row=13, column=6, sticky="E", padx = 10)

        print("End of Battery Page")

def simulStart():
    global gSimulFlag
    gSimulFlag = 1

def calcSamplePath(lam,mu, n):
    numArr = lam.shape[0]
    numDep = mu.shape[0]
    path = np.zeros(shape=(numArr+numDep,3))
    path[0:numArr,0] = lam.ravel()
    path[0:numArr, 1] = +1
    path[numArr:(numArr + numDep),0] = mu.ravel()
    path[numArr:(numArr + numDep), 1] = -1
    path = path[path[:, 0].argsort()]
    path[0,2] = max(0,path[0,1]) #start of sample path
    for i in range(numArr+numDep-1):
        path[i+1,2] = max(0,path[i,2]+path[i+1,1])
    path = path[path[:,0]<=n,:]
    return path[:,0], path[:,2]

def animate(i):
    global gSimulFlag
    global aBattery1
    global gArrChoice
    global gServChoice
    global gNumPeriods
    global gArr1, gArr21, gArr22, gArr31, gArr32, gArr41, gArr42
    global gProc1, gProc21, gProc22, gProc31, gProc32, gProc41, gProc42

    #Leave animate function if no new simulation has been requested, to not constantly update the graph
    if gSimulFlag == 0:
        return

    n = gNumPeriods.get()
    #Select correct interarrival time
    if gArrChoice.get() == 1:
        lam = np.random.exponential(scale=gArr1.get(), size = int((2*n)/(gArr1.get()+0.1))).reshape(-1,1)
    elif gArrChoice.get() == 2:
        lam = np.random.normal(loc=gArr21.get(), scale=gArr22.get(), size = int((2*n)/(gArr21.get()+0.1) + (2*n) / gArr22.get())).reshape(-1,1)
        lam = lam*(lam>0)
    elif gArrChoice.get() == 3:
        lam = np.random.uniform(low=gArr31.get(), high = gArr32.get(), size = int((2*n)/((gArr31.get()+gArr32.get())/2))).reshape(-1,1)
        lam = lam * (lam > 0)
    else:
        lam = np.random.exponential(size= int((2*gArr42.get()*n)/gArr41.get())).reshape(-1,gArr42.get()) #Erlang
        lam = np.sum(lam,axis=1).reshape(-1,1)

    # Select correct processing time
    if gServChoice.get() == 1:
        mu = np.random.exponential(scale=gProc1.get(), size = int((2*n)/(gProc1.get()+0.1))).reshape(-1,1)
    elif gServChoice.get() == 2:
        mu = np.random.normal(loc=gProc21.get(), scale=gProc22.get(), size = int((2*n)/(gProc21.get()+0.1) + (2*n) / gProc22.get())).reshape(-1,1)
        mu = mu*(mu>0)
    elif gServChoice.get() == 3:
        mu = np.random.uniform(low=gProc31.get(), high = gProc32.get(), size = int((2*n)/((gProc31.get()+gProc32.get())/2))).reshape(-1,1)
        mu = mu * (mu > 0)
    else:
        mu = np.random.exponential(size= int((2*gProc42.get()*n)/gProc41.get())).reshape(-1,gProc42.get()) #Erlang
        mu = np.sum(mu,axis=1).reshape(-1,1)

    lam = np.array(np.cumsum(lam)).reshape(-1,1) #-1 is unspecified and just fills to how ever many rows we have
    mu = np.array(np.cumsum(mu)).reshape(-1, 1)

    x, y = calcSamplePath(lam,mu,n)


    aBattery1.clear()
    aBattery1.step(list(x), list(y))

    gSimulFlag = 0

    print(gArrChoice.get())

#Actually run the interface
app = BatSimGUI()
app.geometry("1280x768")  # Set size
ani = animation.FuncAnimation(fBattery, animate, interval = 1000) #live graph functionality
app.mainloop() #Make Tkinter window stay in front and not instantly close
import tkinter as tk
from tkinter import ttk
import matplotlib
matplotlib.use("TkAgg") #make sure you use the tkinter backend
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib import style
import numpy as np
import pandas as pd

#Define constants for fonts
LARGE_FONT = {"Verdana", 12}
style.use("ggplot")

class BatSimGUI(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs) #Start up the tk.TK class

        #Overall properties
        self.geometry("1024x768")  # Set size
        tk.Tk.iconbitmap(self, default="battery_icon.ico") #sets icon top left
        tk.Tk.wm_title(self, "PhD or Bust")

        container = tk.Frame(self) #This is where everything lives
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1) #Defines row minimum height and importance
        container.grid_columnconfigure(0, weight=1) #Defines column minimum height and importance

        self.frames = {} #stores all my frames

        for F in (StartPage, BatteryPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew") #where to place this frame on the grid and how to align it

        self.show_frame(StartPage) # What to show upon initialization

    def show_frame(self, cont): #bring to the the front, whatever frame with the key passed into cont is there
        frame = self.frames[cont]
        frame.tkraise()

def testfunc(stringToPrint):
    print(stringToPrint)


f = Figure(figsize=(3, 3), dpi=100)
a = f.add_subplot(111)  # 1 by 1 chart and this is the first chart

def animate(i):
    pullData = open("sampleData.txt","r").read()
    dataList = pullData.split("\n")
    xList = []
    yList = []
    for eachLine in dataList:
        if len(eachLine) > 1:
            x, y = eachLine.split(",")
            xList.append(int(x))
            yList.append(int(y))

    a.clear() #before we do anything with the Data, clear current RAM
    a.plot(xList, yList)


class StartPage(tk.Frame):

    def __init__(self, parent, controller): #initializes a new class in a container
        tk.Frame.__init__(self, parent)

        label = ttk.Label(self, text="Use Simulator at own risk", font = LARGE_FONT) # create a label
        label.pack(padx=10, pady=10) #add the label to the window

        button1 = ttk.Button(self, text="Battery Graph Page",
                             command=lambda: controller.show_frame(BatteryPage)) #needs the lambda, or doesnt work properly
        button1.pack()

        button2 = ttk.Button(self, text="Exit",
                             command=quit) #needs the lambda, or doesnt work properly
        button2.pack()


class BatteryPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        label = ttk.Label(self, text="Battery Page", font=LARGE_FONT)  # create a label
        label.pack(padx=10, pady=10)  # add the label to the window

        rateChoice = tk.IntVar()
        radio1 = ttk.Radiobutton(self, variable=rateChoice, text="Uniform", value=1,
                                 command= lambda: print(rateChoice.get())).pack()
        radio2 = ttk.Radiobutton(self, variable=rateChoice, text="Normal", value=2,
                                 command= lambda: print(rateChoice.get())).pack()
        radio3 = ttk.Radiobutton(self, variable=rateChoice, text="Poisson", value=3,
                                 command= lambda: print(rateChoice.get())).pack()
        radio4 = ttk.Radiobutton(self, variable=rateChoice, text="Erlang", value=4,
                                 command= lambda: print(rateChoice.get())).pack()

        #Add Graph

        canvas = FigureCanvasTkAgg(f, self) #transforms figure into tkinter thing
        canvas.draw() #Show the plot
        canvas.get_tk_widget().pack(side = tk.BOTTOM, fill=tk.BOTH, expand=True) #Add the plot to the graph

        toolbar = NavigationToolbar2Tk(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side = tk.TOP, fill=tk.BOTH, expand=True)

        #Add final button
        button1 = ttk.Button(self, text="Back To Home",
                            command=lambda: controller.show_frame(StartPage))  # needs the lambda, or doesnt work properly
        button1.pack( side=tk.TOP)

app = BatSimGUI()
ani = animation.FuncAnimation(f, animate, interval = 1000) #live graph functionality
app.mainloop() #Make Tkinter window stay in front and not instantly close
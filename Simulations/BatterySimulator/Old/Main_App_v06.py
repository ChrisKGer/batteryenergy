import sys
from PyQt5 import QtCore, QtGui, QtWidgets

import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

from Classes.winSamplePath import WinSamplePath
from Classes.winSwitchedPoisson import WinSwitchedPoisson

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CenterScreen(object):
    def setupUi(self, CenterScreen):
        CenterScreen.setObjectName("CenterScreen")
        CenterScreen.resize(1152, 700)
        self.centralwidget = QtWidgets.QWidget(CenterScreen)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.btnOption1 = QtWidgets.QPushButton(self.centralwidget)
        self.btnOption1.setObjectName("btnOption1")
        self.gridLayout.addWidget(self.btnOption1, 0, 1, 1, 1)
        self.btnOption2 = QtWidgets.QPushButton(self.centralwidget)
        self.btnOption2.setObjectName("btnOption2")
        self.gridLayout.addWidget(self.btnOption2, 1, 1, 1, 1)
        CenterScreen.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(CenterScreen)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1152, 21))
        self.menubar.setObjectName("menubar")
        CenterScreen.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(CenterScreen)
        self.statusbar.setObjectName("statusbar")
        CenterScreen.setStatusBar(self.statusbar)
        self.menu_SamplePath = QtWidgets.QAction(CenterScreen)
        self.menu_SamplePath.setObjectName("menu_SamplePath")
        self.menu_SwitchedPoisson = QtWidgets.QAction(CenterScreen)
        self.menu_SwitchedPoisson.setObjectName("menu_SwitchedPoisson")

        self.retranslateUi(CenterScreen)
        QtCore.QMetaObject.connectSlotsByName(CenterScreen)

        #Own functionality
        self.dialogs = list()
        self.btnOption1.clicked.connect(self.go_to_SamplePath)

    def retranslateUi(self, CenterScreen):
        _translate = QtCore.QCoreApplication.translate
        CenterScreen.setWindowTitle(_translate("CenterScreen", "MainWindow"))
        self.btnOption1.setText(_translate("CenterScreen", "Sample Path"))
        self.btnOption2.setText(_translate("CenterScreen", "Switched Poisson Process"))
        self.menu_SamplePath.setText(_translate("CenterScreen", "Sample Path"))
        self.menu_SwitchedPoisson.setText(_translate("CenterScreen", "Switched Poisson"))

    def go_to_SamplePath(self):
        self.dummyWindow = QtWidgets.QMainWindow()
        self.dummyUi = WinSamplePath()
        self.dummyUi.setupUi(self.dummyWindow)
        self.dummyWindow.show()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    CenterScreen = QtWidgets.QMainWindow()
    ui = Ui_CenterScreen()
    ui.setupUi(CenterScreen)
    CenterScreen.show()
    sys.exit(app.exec_())


import sys
from PyQt5 import QtCore, QtGui, QtWidgets

from Classes.winSamplePath import WinSamplePath
from Classes.winSwitchedPoisson import WinSwitchedPoisson

class CenterScreen(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1152, 685)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setGeometry(QtCore.QRect(0, 0, 1151, 648))
        self.stackedWidget.setObjectName("stackedWidget")

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1152, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionSample_Path = QtWidgets.QAction(MainWindow)
        self.actionSample_Path.setObjectName("actionSample_Path")
        self.actionSwitched_Poisson = QtWidgets.QAction(MainWindow)
        self.actionSwitched_Poisson.setObjectName("actionSwitched_Poisson")
        self.menuFile.addAction(self.actionSample_Path)
        self.menuFile.addAction(self.actionSwitched_Poisson)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        #Own Code
        #Add Pages
        self.page = QtWidgets.QMainWindow()
        self.page.resize(1152, 648)
        self.page.setObjectName("page")
        self.dummyUi = WinSamplePath()
        self.dummyUi.setupUi(self.page)
        self.stackedWidget.addWidget(self.page)

        self.page_2 = QtWidgets.QMainWindow()
        self.page_2.setObjectName("page_2")
        self.dummyUi2 = WinSwitchedPoisson()
        self.dummyUi2.setupUi(self.page_2)
        self.stackedWidget.addWidget(self.page_2)

        #Add Menu Functionality
        self.actionSample_Path.triggered.connect(lambda: self.stackedWidget.setCurrentIndex(0))
        self.actionSwitched_Poisson.triggered.connect(lambda: self.stackedWidget.setCurrentIndex(1))

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menuFile.setTitle(_translate("MainWindow", "Modules"))
        self.actionSample_Path.setText(_translate("MainWindow", "Sample Path"))
        self.actionSwitched_Poisson.setText(_translate("MainWindow", "Switched Poisson"))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = CenterScreen()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

